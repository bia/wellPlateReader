package plugins.adufour.hcs.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import danyfel80.wells.ui.WellPlateViewer;
import icy.gui.frame.IcyFrame;
import icy.gui.viewer.Viewer;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.util.StringUtil;
import plugins.adufour.hcs.data.OldField;
import plugins.adufour.hcs.data.OldWell;
import plugins.adufour.hcs.data.OldWellPlate;

/**
 * @deprecated Use {@link WellPlateViewer} instead.
 * @author Daniel Felipe Gonzalez Obando
 */
@Deprecated
public class OldWellPlateViewer extends IcyFrame
{
    public OldWellPlateViewer(final OldWellPlate wellPlate)
    {
        super(wellPlate.toString(), false, true, false, true, true);

        //////////////////////
        // GENERAL LAYOUT: //
        // 1 2 3 4 | __ //
        // a o o o o | / \ //
        // b o o o o | \__/ //
        // c o o o o | //
        //////////////////////

        setLayout(new BorderLayout());

        ////////////////////////
        // CENTER: PLATE VIEW //
        ////////////////////////

        final JPanel plateView = new JPanel(new GridLayout(wellPlate.getNbRows() + 1, wellPlate.getNbCols() + 1, 2, 2));
        plateView.setPreferredSize(new Dimension(425, 300));
        add(plateView, BorderLayout.CENTER);

        ////////////////////////////
        // EAST: SINGLE WELL VIEW //
        ////////////////////////////

        JPanel singleWellPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        singleWellPanel.setPreferredSize(new Dimension(220, 300));

        // SELECTED WELL (JLabel)
        // WELL (WellViewer)
        // SELECTED FIELD (JLabel)
        // FIELD SLIDER (JSlider)

        final JLabel selectedWellLabel = new JLabel("No well selected");
        final JLabel selectedFieldLabel = new JLabel("");
        final JSlider fieldSlider = new JSlider(JSlider.HORIZONTAL);
        fieldSlider.setVisible(false);
        fieldSlider.setBorder(new EmptyBorder(5, 10, 5, 15));

        final OldWellViewer selectedWellViewer = new OldWellViewer(null, true)
        {
            private static final long serialVersionUID = -7385257944014813154L;

            @Override
            protected void fieldChanged(OldWell well, OldField field)
            {
                try
                {
                    Sequence sequence = wellPlate.loadField(field);
                    selectedFieldLabel.setText("Field " + field.getID());
                    fieldSlider.setValue(field.getID());
                    if (sequence.getFirstViewer() == null)
                        new Viewer(sequence);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    throw new IcyHandledException("Cannot load well " + well.getAlphanumericID(), e);
                }
            }
        };
        selectedWellViewer.setPreferredSize(new Dimension(220, 220));

        fieldSlider.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                OldWell well = selectedWellViewer.getWell();
                if (well == null || well.isEmpty())
                    return;

                int sliderValue = fieldSlider.getValue();
                if (sliderValue != selectedWellViewer.getSelectedFieldID())
                {
                    selectedWellViewer.setSelectedField(well.getField(sliderValue));
                }
            }
        });

        singleWellPanel.add(selectedWellLabel);
        singleWellPanel.add(selectedWellViewer);
        singleWellPanel.add(selectedFieldLabel);
        singleWellPanel.add(fieldSlider);
        add(singleWellPanel, BorderLayout.EAST);

        /////////////////
        // PLATE LOGIC //
        /////////////////

        int nbRows = wellPlate.getNbRows();
        int nbCols = wellPlate.getNbCols();
        // Header row
        for (int col = 0; col <= nbCols; col++)
        {
            JLabel header = new JLabel(col == 0 ? "" : StringUtil.toString(col, 2), JLabel.CENTER);
            header.setFont(header.getFont().deriveFont(header.getFont().getSize2D() - 2f));
            plateView.add(header);
        }
        for (int row = 1; row <= nbRows; row++)
            for (int col = 0; col <= nbCols; col++)
                if (col == 0)
                {
                    char colHeader = 'A';
                    colHeader += (row - 1);
                    JLabel header = new JLabel("" + colHeader, JLabel.CENTER);
                    header.setFont(header.getFont().deriveFont(header.getFont().getSize2D() - 2f));
                    plateView.add(header);
                }
                else
                {
                    final OldWell well = wellPlate.getWellAt(row - 1, col - 1);
                    OldWellViewer wellViewInPlate = new OldWellViewer(well, false);
                    wellViewInPlate.addMouseListener(new MouseAdapter()
                    {
                        @Override
                        public void mouseClicked(MouseEvent e)
                        {
                            String wellID = "Well " + well.getAlphanumericID();
                            selectedWellLabel.setText(wellID);
                            selectedWellViewer.setWell(well);
                            fieldSlider.setVisible(!well.isEmpty());
                            if (!well.isEmpty())
                            {
                                int firstFieldID = well.fieldIterator().next().getID();
                                fieldSlider.setMinimum(firstFieldID);
                                int lastFieldID = well.getFields().size();
                                if (firstFieldID == 0)
                                    lastFieldID--;
                                fieldSlider.setMaximum(lastFieldID);
                            }

                            // Highlight this well in the grid
                            for (Component component : plateView.getComponents())
                            {
                                if (!(component instanceof OldWellViewer))
                                    continue;
                                OldWellViewer wellViewer = (OldWellViewer) component;
                                wellViewer.setSelected(wellViewer.getWell().equals(well));
                            }
                            plateView.repaint();
                        }
                    });
                    plateView.add(wellViewInPlate);
                }
        repaint();
    }
}