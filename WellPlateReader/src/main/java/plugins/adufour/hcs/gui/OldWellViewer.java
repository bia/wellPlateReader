package plugins.adufour.hcs.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.RectangularShape;

import javax.swing.JComponent;

import danyfel80.wells.ui.WellViewer;
import plugins.adufour.hcs.data.OldField;
import plugins.adufour.hcs.data.OldWell;
import plugins.adufour.hcs.data.OldWell.Shape;

/**
 * @deprecated Use {@link WellViewer} instead.
 * @author Daniel Felipe Gonzalez Obando
 */
@Deprecated
public class OldWellViewer extends JComponent implements MouseListener
{
    private static final long serialVersionUID = 2478991498042362510L;

    private OldWell well;

    /**
     * Indicates whether the well is selected and should be highlighted in the
     * grid view
     */
    private boolean selected;

    private boolean showFields;

    private int selectedFieldID = 1;

    /**
     * The transform to the well's upper left hand corner (unit: pixels)
     */
    private final AffineTransform transform = new AffineTransform();

    public OldWellViewer(OldWell well, boolean showFields)
    {
        this.well = well;
        this.showFields = showFields;
        addMouseListener(this);

        // if the well has fields inside it, select the first one
        if (well != null && !well.isEmpty())
            setSelectedField(well.getFields().iterator().next());
    }

    @Override
    protected void processMouseEvent(MouseEvent e)
    {
        if (well == null)
        {
            e.consume();
            return;
        }

        // Restrict mouse events to the well shape
        try
        {
            Point2D clickLocation = transform.inverseTransform(e.getPoint(), null);
            if (well.getShape().wellShape.contains(clickLocation))
                super.processMouseEvent(e);
        }
        catch (NoninvertibleTransformException e1)
        {
            e1.printStackTrace();
        }
    }

    /**
     * Called when a field is clicked inside this well. The default implementation
     * is empty and should be overridden to handle such events
     * 
     * @param theWell
     *        the well in which a field has been clicked
     * @param theField
     *        the field that has been clicked
     */
    protected void fieldChanged(OldWell theWell, OldField theField)
    {

    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        if (well == null)
            return;

        Graphics2D g2 = (Graphics2D) g.create();

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setStroke(new BasicStroke(0.6f / (float) Math.log1p(transform.getScaleX())));
        g2.transform(transform);
        g2.setColor(Color.gray);

        RectangularShape shape = well.getShape().wellShape;
        g2.draw(shape);
        if (!well.isEmpty())
            g2.fill(shape);

        // Draw fields inside the well?
        if (showFields)
        {
            for (OldField field : well.getFields())
            {
                g2.setColor(field.getID() == selectedFieldID ? Color.yellow : Color.darkGray);
                g2.fill(field.getBounds());
            }
        }
        else if (selected)
        {
            g2.setStroke(new BasicStroke(2f / (float) Math.log1p(transform.getScaleX())));
            g2.setColor(Color.yellow);
            g2.draw(shape);
        }

        g2.dispose();
    }

    @Override
    public void setBounds(int x, int y, int w, int h)
    {
        if (well != null)
        {
            // Adjust the local transform
            transform.setToIdentity();
            Shape wellShape = well.getShape();
            transform.translate(0.05 * w, 0.05 * h);
            double scaleX = 0.9 * w / wellShape.wellShape.getWidth();
            double scaleY = 0.9 * h / wellShape.wellShape.getHeight();
            if (scaleX > 0 && scaleY > 0)
                transform.scale(scaleX, scaleY);
        }

        super.setBounds(x, y, w, h);
    }

    public void setWell(OldWell well)
    {
        this.well = well;

        if (well == null || well.isEmpty())
        {
            repaint();
        }
        else
        {
            setSelectedField(well.fieldIterator().next());
        }
    }

    public OldWell getWell()
    {
        return well;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public int getSelectedFieldID()
    {
        return selectedFieldID;
    }

    public void setSelectedField(OldField selectedField)
    {
        this.selectedFieldID = selectedField.getID();
        fieldChanged(well, selectedField);
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        try
        {
            Point2D clickLocation = transform.inverseTransform(e.getPoint(), null);

            // Check if a field was clicked
            if (showFields && e.getClickCount() == 1)
            {
                for (OldField field : well.getFields())
                    if (field.getBounds().contains(clickLocation))
                    {
                        setSelectedField(field);
                        break;
                    }
            }
        }
        catch (NoninvertibleTransformException nitE)
        {
        }
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
    }
}