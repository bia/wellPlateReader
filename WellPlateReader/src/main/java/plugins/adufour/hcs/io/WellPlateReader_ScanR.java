package plugins.adufour.hcs.io;

import java.io.File;
import java.util.concurrent.Future;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IPlate;
import danyfel80.wells.data.IWell;
import danyfel80.wells.data.scanr.ScanRPlate;
import danyfel80.wells.util.MessageProgressListener;
import icy.sequence.Sequence;

/**
 * Specialized well plate reader for the Scan^R system (Columbus)
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class WellPlateReader_ScanR extends AbstractWellPlateReader
{

    @Override
    public String getSystemName()
    {
        return "ScanR (Olympus)";
    }

    @Override
    public boolean isValidPlate(File file)
    {
        // TODO remove when scan r is implemented.
        return false;
        // if (!file.isDirectory())
        // return false;
        //
        // boolean hasData = false;
        // boolean hasLog = false;
        // boolean hasDesc = false;
        // boolean hasDescXML = false;
        //
        // // Check the directory contents
        // for (File directoryFile : file.listFiles())
        // {
        // // "data" folder
        // if (directoryFile.getName().equalsIgnoreCase("data"))
        // {
        // hasData = true;
        // }
        // // AcquisitionLog.dat
        // else if (directoryFile.getName().equalsIgnoreCase("AcquisitionLog.dat"))
        // {
        // hasLog = true;
        // }
        // // experiment_descriptor.dat
        // else if (directoryFile.getName().equalsIgnoreCase("experiment_descriptor.dat"))
        // {
        // hasDesc = true;
        // }
        // // experiment_descriptor.xml
        // else if (directoryFile.getName().equalsIgnoreCase("experiment_descriptor.xml"))
        // {
        // hasDescXML = true;
        // }
        // }
        //
        // return hasData && hasDesc && hasDescXML && hasLog;
    }

    @Override
    public Future<ScanRPlate> loadPlateFromFolder(File folder, MessageProgressListener progressListener)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Future<? extends Sequence> loadField(IPlate plate, IWell well, IField field, Sequence sequence,
            MessageProgressListener progressListener)
    {
        // TODO Auto-generated method stub
        return null;
    }

}
