package plugins.adufour.hcs.io;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.media.jai.RenderedImageAdapter;

import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.TIFFDecodeParam;
import com.sun.media.jai.codecimpl.TIFFCodec;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IPlate;
import danyfel80.wells.data.IWell;
import danyfel80.wells.data.opera.OperaChannel;
import danyfel80.wells.data.opera.OperaField;
import danyfel80.wells.data.opera.OperaPlane;
import danyfel80.wells.data.opera.OperaPlate;
import danyfel80.wells.data.opera.OperaTimepoint;
import danyfel80.wells.util.MessageProgressListener;
import danyfel80.wells.util.stream.StreamUtils;
import icy.file.FileUtil;
import icy.image.IcyBufferedImage;
import icy.image.colormap.IcyColorMap;
import icy.image.colormap.LinearColorMap;
import icy.sequence.Sequence;
import icy.type.dimension.Dimension2D;
import ome.xml.meta.OMEXMLMetadata;
import ome.xml.model.primitives.PositiveInteger;

/**
 * Specialized well plate reader for the Opera system (Evotec PerkinElmer)
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class WellPlateReader_EvotecPerkinElmerOperaFlex extends AbstractWellPlateReader
{

    @Override
    public String getSystemName()
    {
        return "Evotec-PerkinElmer Opera Flex";
    }

    private final static java.io.FileFilter FLEX_FILE_FILTER = new java.io.FileFilter()
    {
        @Override
        public boolean accept(File pathname)
        {
            return FileUtil.getFileExtension(pathname.getName(), false).equalsIgnoreCase("flex");
        }
    };

    private final static java.io.FileFilter COLUMBUS_FILE_FILTER = new java.io.FileFilter()
    {
        @Override
        public boolean accept(File pathname)
        {
            return pathname.getName().endsWith(".meax") || pathname.getName().endsWith(".ColumbusIDX.xml");
        }
    };

    @Override
    public boolean isValidPlate(File folder)
    {
        boolean containsFlexFiles = FileUtil.getFiles(folder.getPath(), FLEX_FILE_FILTER, true, false,
                false).length > 0;
        boolean hasColombusIndexFile = FileUtil.getFiles(folder.getPath(), COLUMBUS_FILE_FILTER, true, false,
                false).length > 0;
        return containsFlexFiles && !hasColombusIndexFile;
    }

    @Override
    public Future<OperaPlate> loadPlateFromFolder(File folder, MessageProgressListener progressListener)
    {
        ExecutorService executor = Executors.newSingleThreadExecutor((Runnable r) -> {
            return new Thread(r, "EvotecFlexWellPlateReader");
        });

        Future<OperaPlate> future = executor.submit(() -> {
            return loadPlateFromFolder_internal(folder, progressListener);
        });

        executor.shutdown();
        return future;
    }

    private OperaPlate loadPlateFromFolder_internal(File folder, MessageProgressListener progressListener)
            throws IOException
    {
        String[] files = FileUtil.getFiles(folder.getPath(), FLEX_FILE_FILTER, true, false, false);
        String descriptorFile = Optional
                .ofNullable(FileUtil.getFiles(folder.getPath(), COLUMBUS_FILE_FILTER, true, false, false))
                .map(fs -> fs.length == 0 ? null : fs[0]).orElse(null);
        if (files.length == 0 || descriptorFile != null)
            throw new IOException("Invalid folder: " + folder.getPath() + ", no valid files in it.");

        OperaPlate plate = new OperaPlate.Builder(folder.getAbsolutePath(), files).progressListener(progressListener)
                .build();

        return plate;
    }

    /**
     * @param field
     *        The field to load. The field must be of type {@link OperaField}.
     */
    @Override
    public Future<? extends Sequence> loadField(IPlate plate, IWell well, IField field, Sequence sequence,
            MessageProgressListener progressListener)
    {
        CompletableFuture<Sequence> future = new CompletableFuture<Sequence>();
        if (!(field instanceof OperaField))
        {
            future.completeExceptionally(
                    new ClassCastException("Provided field is not of type " + OperaField.class.getName()));
            return future;
        }
        if (!(plate instanceof OperaPlate))
        {
            future.completeExceptionally(
                    new ClassCastException("Provided plate is not of type " + OperaPlate.class.getName()));
            return future;
        }
        if (sequence == null)
        {
            sequence = new Sequence();
        }
        sequence.beginUpdate();

        OperaPlate operaPlate = (OperaPlate) plate;
        OMEXMLMetadata metadata = sequence.getOMEXMLMetadata();
        metadata.setInstrumentID(operaPlate.getDevice(), 0);
        metadata.setPlateName(operaPlate.getName(), 0);
        metadata.setPlateID(operaPlate.getBarcode(), 0);
        metadata.setPlateRows(new PositiveInteger(operaPlate.getDimension().height), 0);
        metadata.setPlateColumns(new PositiveInteger(operaPlate.getDimension().width), 0);
        // metadata.setWellColumn(new NonNegativeInteger(well.getPositionInPlate().x), 0, (int) well.getId());
        // metadata.setWellRow(new NonNegativeInteger(well.getPositionInPlate().y), 0, (int) well.getId());

        sequence.setPositionX(field.getPosition().getX());
        sequence.setPositionY(field.getPosition().getY());
        Dimension2D.Double pixelSize = new Dimension2D.Double();
        Map<Integer, String> channelNames = new HashMap<>();
        Map<Integer, IcyColorMap> channelColors = new HashMap<>();

        try
        {
            loadPlanes(operaPlate, (OperaField) field, sequence, pixelSize, channelNames, channelColors);

            sequence.setPixelSizeX(pixelSize.getSizeX());
            sequence.setPixelSizeY(pixelSize.getSizeY());
            for (Entry<Integer, String> entry : channelNames.entrySet())
            {
                sequence.setChannelName(entry.getKey(), entry.getValue());
            }
            for (Entry<Integer, IcyColorMap> entry : channelColors.entrySet())
            {
                sequence.setColormap(entry.getKey(), entry.getValue(), true);
            }
        }
        finally
        {
            sequence.endUpdate();
        }
        future.complete(sequence);

        return future;
    }

    private void loadPlanes(OperaPlate plate, OperaField field, Sequence sequence, Dimension2D.Double pixelSize,
            Map<Integer, String> channelNames, Map<Integer, IcyColorMap> channelColors)
    {
        List<List<IcyBufferedImage>> planes = field.getPlanes().values().stream()
                .sorted(Comparator.comparingDouble(OperaPlane::getPositionZ))
                .map(StreamUtils.wrapFunction(
                        plane -> loadPlane(sequence, pixelSize, channelNames, channelColors, plate, field, plane)))
                .collect(Collectors.toList());

        int z = 0, t;
        for (Iterator<List<IcyBufferedImage>> itZ = planes.iterator(); itZ.hasNext();)
        {
            List<IcyBufferedImage> planeImages = (List<IcyBufferedImage>) itZ.next();
            t = 0;
            for (Iterator<IcyBufferedImage> itT = planeImages.iterator(); itT.hasNext();)
            {
                IcyBufferedImage image = (IcyBufferedImage) itT.next();
                sequence.setImage(t, z, image);
                t++;
            }
            z++;
        }
    }

    private List<IcyBufferedImage> loadPlane(Sequence sequence, Dimension2D.Double pixelSize,
            Map<Integer, String> channelNames, Map<Integer, IcyColorMap> channelColors, OperaPlate plate,
            OperaField field, OperaPlane plane)
    {
        List<IcyBufferedImage> timeImages = plane.getTimepoints().values().stream()
                .sorted(Comparator.comparingLong(OperaTimepoint::getId))
                .map(StreamUtils.wrapFunction(
                        t -> loadTimePoint(sequence, pixelSize, channelNames, channelColors, plate, field, plane, t)))
                .collect(Collectors.toList());
        return timeImages;
    }

    private IcyBufferedImage loadTimePoint(Sequence sequence, Dimension2D.Double pixelSize,
            Map<Integer, String> channelNames, Map<Integer, IcyColorMap> channelColors, OperaPlate plate,
            OperaField field, OperaPlane plane, OperaTimepoint t)
    {
        List<IcyBufferedImage> channelImages = t.getChannels().values().stream()
                .sorted(Comparator.comparingLong(OperaChannel::getId))
                .map(StreamUtils.wrapFunction(ch -> loadChannel(sequence, pixelSize, channelNames, channelColors, plate,
                        field, plane, t, ch)))
                .collect(Collectors.toList());
        return IcyBufferedImage.createFrom(channelImages);
    }

    private IcyBufferedImage loadChannel(Sequence sequence, Dimension2D.Double pixelSize,
            Map<Integer, String> channelNames, Map<Integer, IcyColorMap> channelColors, OperaPlate plate,
            OperaField field, OperaPlane plane, OperaTimepoint t, OperaChannel ch) throws IOException
    {
        String folderPath = plate.getFolder();
        String flexFile = ch.getImage().getUrl();
        File fullFilePath = Paths.get(folderPath, flexFile).toFile();

        channelNames.put((int) ch.getId(), ch.getName());
        IcyColorMap colorMap = new LinearColorMap(((int) ch.getEmissionWavelength()) + "nm", ch.getColor());
        channelColors.put((int) ch.getId(), colorMap);
        pixelSize.setSize(ch.getImage().getResolutionX(), ch.getImage().getResolutionY());

        // Bypass BioFormats for faster access (!!)
        SeekableStream ss = SeekableStream.wrapInputStream(new FileInputStream(fullFilePath.getAbsoluteFile()), true);
        ImageDecoder dec = TIFFCodec.createImageDecoder("tiff", ss, new TIFFDecodeParam());
        RenderedImage loadedImage = dec.decodeAsRenderedImage((int) ch.getId());
        return IcyBufferedImage.createFrom(new RenderedImageAdapter(loadedImage));
    }

}
