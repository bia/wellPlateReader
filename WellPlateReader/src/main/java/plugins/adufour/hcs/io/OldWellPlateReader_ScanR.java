package plugins.adufour.hcs.io;

import java.awt.geom.Point2D;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.media.jai.RenderedImageAdapter;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.TIFFDecodeParam;
import com.sun.media.jai.codecimpl.TIFFCodec;

import icy.gui.frame.progress.CancelableProgressFrame;
import icy.image.IcyBufferedImage;
import icy.sequence.MetaDataUtil;
import icy.sequence.Sequence;
import icy.util.OMEUtil;
import icy.util.XMLUtil;
import ome.xml.meta.OMEXMLMetadata;
import ome.xml.meta.OMEXMLMetadataRoot;
import ome.xml.model.Image;
import ome.xml.model.OME;
import ome.xml.model.OMEModelImpl;
import ome.xml.model.Pixels;
import ome.xml.model.enums.EnumerationException;
import ome.xml.model.primitives.PositiveInteger;
import plugins.adufour.hcs.data.OldField;
import plugins.adufour.hcs.data.OldWell;
import plugins.adufour.hcs.data.OldWell.Shape;
import plugins.adufour.hcs.data.OldWellPlate;

/**
 * @deprecated Use {@link WellPlateReader_ScanR} instead.
 * @author Daniel Felipe Gonzalez Obando
 */
@Deprecated
public class OldWellPlateReader_ScanR extends OldWellPlateReader
{
    @Override
    public String getSystemName()
    {
        return "ScanR (Olympus)";
    }

    @Override
    public boolean isValidPlate(File plateFile)
    {
        return false;
        // if (!plateFile.isDirectory())
        // return false;
        //
        // boolean hasData = false;
        // boolean hasLog = false;
        // boolean hasDesc = false;
        // boolean hasDescXML = false;
        //
        // // Check the directory contents
        // for (File file : plateFile.listFiles()) {
        // // "data" folder
        // if (file.getName().equalsIgnoreCase("data")) {
        // hasData = true;
        // }
        // // AcquisitionLog.dat
        // else if (file.getName().equalsIgnoreCase("AcquisitionLog.dat")) {
        // hasLog = true;
        // }
        // // experiment_descriptor.dat
        // else if (file.getName().equalsIgnoreCase("experiment_descriptor.dat")) {
        // hasDesc = true;
        // }
        // // experiment_descriptor.xml
        // else if (file.getName().equalsIgnoreCase("experiment_descriptor.xml")) {
        // hasDescXML = true;
        // }
        // }
        //
        // return hasData && hasDesc && hasDescXML && hasLog;
    }

    @Override
    public OldWellPlate loadPlateFromFolder(File folder, Optional<CancelableProgressFrame> progress) throws IOException
    {
        OMEXMLMetadata metadata = OMEUtil.createOMEXMLMetadata();

        OldWellPlate wellPlate = null;

        Point2D wellA1 = new Point2D.Double();
        int wellShiftX = 0, wellShiftY = 0;
        double wellDiameter = 0;
        int totalStageX = 0, totalStageY = 0;

        // Step 1: read plate info from experiment_descriptor.xml
        try
        {
            Document xml = XMLUtil.createDocument(
                    FileUtils.readFileToString(new File(folder.getPath() + "/experiment_descriptor.xml"), "UTF-8"));

            Element rootCluster = XMLUtil.getElement(XMLUtil.getElement(xml, "LVData"), "Cluster");

            // Browse <String> nodes to get the plate name
            for (Element string : XMLUtil.getElements(rootCluster, "String"))
            {
                String name = XMLUtil.getElementValue(string, "Name", "");
                if (name.equalsIgnoreCase("plate name"))
                {
                    String plateName = XMLUtil.getElementValue(string, "Val", "Unnamed plate");
                    metadata.setPlateID(plateName, 0);
                }
            }

            Shape wellShape = null;
            // Browse the <Cluster> node containing the plate info
            for (Element cluster : XMLUtil.getElements(rootCluster, "Cluster"))
            {
                String name = XMLUtil.getElementValue(cluster, "Name", "");

                if (!name.equalsIgnoreCase("format typedef"))
                    continue;

                // We have the right cluster => browse the contents

                for (Element property : XMLUtil.getElements(cluster))
                {
                    if (property.getNodeName().contentEquals("Name"))
                        continue;
                    if (property.getNodeName().contentEquals("NumElts"))
                        continue;

                    String value = XMLUtil.getElementValue(property, "Val", "");

                    switch (XMLUtil.getElementValue(property, "Name", ""))
                    {
                        case "Rows":
                            metadata.setPlateRows(new PositiveInteger(Integer.parseInt(value)), 0);
                            break;
                        case "Columns":
                            metadata.setPlateColumns(new PositiveInteger(Integer.parseInt(value)), 0);
                            break;
                        case "well diameter":
                            wellDiameter = Integer.parseInt(value);
                            break;
                        case "Format description":
                            metadata.setPlateName(value, 0);
                            break;
                        case "Well Shape":
                            wellShape = Shape.valueOf(value);
                            break;
                        case "column spacer":
                            wellShiftX = Integer.parseInt(value);
                            break;
                        case "row spacer":
                            wellShiftY = Integer.parseInt(value);
                            break;
                        case "Xtot":
                            totalStageX = Integer.parseInt(value);
                            break;
                        case "Ytot":
                            totalStageY = Integer.parseInt(value);
                            break;
                        case "XYA1": // coordinate of the first well (0,0 is the plate corner)
                            List<Element> coordsA1 = XMLUtil.getElements(property, "I32");
                            int xA1 = XMLUtil.getElementIntValue(coordsA1.get(0), "Val", 0);
                            int yA1 = XMLUtil.getElementIntValue(coordsA1.get(1), "Val", 0);
                            wellA1.setLocation(xA1, yA1);
                            break;
                        default: // don't care
                    }
                }
            }

            wellShape.setDimension(wellDiameter, wellDiameter);

            wellPlate = new OldWellPlate(this, metadata, wellShape);
        }
        catch (SAXException e)
        {
        }

        // Step 2: Read field layout in wells from experiment_descriptor.dat
        try (BufferedReader reader = new BufferedReader(
                new FileReader(folder.getPath() + "/experiment_descriptor.dat")))
        {
            while (reader.ready())
            {
                String line = reader.readLine();
                if (!line.startsWith("IMAGE"))
                    continue;
                String[] tokens = line.split("\t");
                // line: IMAGE X Y AF ID PLATE WELL SUBPOS TIME

                int wellIndex = Integer.parseInt(tokens[6].substring(5));
                int nCols = metadata.getPlateColumns(0).getValue();
                int row = (wellIndex / nCols);
                int col = (wellIndex % nCols) - 1;
                OldWell well = wellPlate.getWellAt(row, col);

                // Field X,Y are given in absolute stage shifts
                double fieldX = (totalStageX - Double.parseDouble(tokens[1].substring(2))) - (col * wellShiftX)
                        - wellA1.getX();
                double fieldY = (totalStageY - Double.parseDouble(tokens[2].substring(2))) - (row * wellShiftY)
                        - wellA1.getY();

                int id = Integer.parseInt(tokens[7].substring(7));

                well.addField(new OldField(well, id, fieldX, fieldY));
            }
        }

        // Step 3: Read the OME metadata file to store individual files
        try
        {
            Document doc = XMLUtil.loadDocument(folder.getPath() + "/data/metadata.ome.xml");
            OME ome = new OMEXMLMetadataRoot(XMLUtil.getElement(doc, "OME"), new OMEModelImpl());
            metadata.setInstrumentID(ome.getCreator(), 0);

            int nbImages = ome.sizeOfImageList();
            for (int i = 0; i < nbImages; i++)
            {
                Image image = ome.getImage(i);
                String id = image.getID(); // e.g. "Image:W14P1"
                int posP = id.indexOf('P');
                int wellIndex = Integer.parseInt(id.substring(7, posP));
                int nCols = metadata.getPlateColumns(0).getValue();
                int row = (wellIndex / nCols);
                int col = (wellIndex % nCols) - 1;
                OldWell well = wellPlate.getWellAt(row, col);

                // Make sure the field exists
                int fieldID = Integer.parseInt(id.substring(posP + 1));
                OldField field = well.getField(fieldID);
                if (field == null)
                    continue;

                Pixels pixels = image.getPixels();
                int imageWidth = pixels.getSizeX().getValue();
                int imageHeight = pixels.getSizeY().getValue();
                double pixelSizeX = pixels.getPhysicalSizeX().value().doubleValue();
                double pixelSizeY = pixels.getPhysicalSizeY().value().doubleValue();
                MetaDataUtil.setPixelSizeX(metadata, 0, pixelSizeX);
                MetaDataUtil.setPixelSizeY(metadata, 0, pixelSizeY);
                double fieldWidth = imageWidth * pixelSizeX;
                double fieldHeight = imageHeight * pixelSizeY;

                field.setFieldSize(fieldWidth, fieldHeight);

                int nChannels = pixels.getSizeC().getValue();
                for (int c = 0; c < nChannels; c++)
                {
                    metadata.setChannelName(pixels.getChannel(c).getName(), c, 0);
                    field.addFilePath(folder.getPath() + "/data/" + pixels.getTiffData(c).getUUID().getFileName());
                }
            }
        }
        catch (EnumerationException e)
        {
        }

        return wellPlate;
    }

    @Override
    public void loadField(OldField field, Sequence sequence) throws IOException
    {
        // Read the individual channels for the current slice
        ArrayList<IcyBufferedImage> channels = new ArrayList<IcyBufferedImage>();

        try
        {
            for (String filePath : field.getFilePaths())
            {
                File file = new File(filePath);
                if (!file.exists())
                {
                    sequence.removeAllImages();
                    return;
                }
                // Bypass BioFormats for faster access (!!)
                SeekableStream ss = SeekableStream.wrapInputStream(new FileInputStream(file), true);
                ImageDecoder dec = TIFFCodec.createImageDecoder("tiff", ss, new TIFFDecodeParam());

                // Read the image data
                RenderedImage _plane = dec.decodeAsRenderedImage(0);
                channels.add(IcyBufferedImage.createFrom(new RenderedImageAdapter(_plane)));
            }

            // The plane can be added to the stack
            sequence.setImage(0, 0, IcyBufferedImage.createFrom(channels)); // FIXME
                                                                            // handle
                                                                            // time-lapse
                                                                            // and 3D
                                                                            // data
        }
        catch (Exception e)
        {
            System.err.println("Warning: cannot load field " + field.toString() + ": " + e.getMessage());
            sequence.removeAllImages();
        }
    }

}
