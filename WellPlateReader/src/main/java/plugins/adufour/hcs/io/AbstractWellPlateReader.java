package plugins.adufour.hcs.io;

import java.io.File;
import java.util.concurrent.Future;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IPlate;
import danyfel80.wells.data.IWell;
import danyfel80.wells.util.MessageProgressListener;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;

/**
 * An abstract well plate reader plugin. Classes implementing this class are both plugins and well plate readers.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public abstract class AbstractWellPlateReader extends Plugin implements PluginLibrary
{
    /**
     * Retrieves the name of the reader, expressing the supported system.
     * 
     * @return A short identifier indicating the supported system
     */
    public abstract String getSystemName();

    /**
     * Checks whether the provided file or directory is valid to be used with this reader.
     * 
     * @param file
     *        The file or folder to check.
     * @return <code>true</code> if the current file or folder is valid for this
     *         plate reader.
     */
    public abstract boolean isValidPlate(File file);

    /**
     * Requests the reader to load the plate on the given file or folder using this reader. A progress listener can be associated to follow the progress of the
     * request.
     * 
     * @param folder
     *        The folder containing the well plate to load.
     * @param progressListener
     *        The progress listener receiving loading status changes.
     * @return A future promise for the loaded well plate.
     */
    public abstract Future<? extends IPlate> loadPlateFromFolder(File folder, MessageProgressListener progressListener);

    /**
     * Loads the specified field into the given sequence. The sequence should be pre-initialized with the proper metadata to avoid having to reload generic
     * plate information for each field.
     * 
     * @param plate
     *        The plate containing the field. Used to fill metadata.
     * @param well
     *        The well containing the field. Used to fill metadata.
     * @param field
     *        The field to load. The type of the field must be compatible with this reader and present on the well plate. Otherwise, the loading will not
     *        succeed.
     * @param sequence
     *        The sequence where the field should be loaded.
     * @param progressListener
     *        The progress listener to follow loading status changes.
     * @return The future promise for the loaded field sequence.
     */
    public abstract Future<? extends Sequence> loadField(IPlate plate, IWell well, IField field, Sequence sequence,
            MessageProgressListener progressListener);
}
