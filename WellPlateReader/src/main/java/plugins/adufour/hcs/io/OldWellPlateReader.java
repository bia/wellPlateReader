package plugins.adufour.hcs.io;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import icy.gui.frame.progress.CancelableProgressFrame;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import icy.sequence.Sequence;
import plugins.adufour.hcs.data.OldField;
import plugins.adufour.hcs.data.OldWellPlate;

/**
 * @deprecated Use {@link AbstractWellPlateReader} instead. Now readers extend from that class.
 * @author Daniel Felipe Gonzalez Obando
 */
@Deprecated
public abstract class OldWellPlateReader extends Plugin implements PluginLibrary
{
    /**
     * @return A short identifier indicating the supported system
     */
    public abstract String getSystemName();

    /**
     * @param file
     *        the file or folder to check
     * @return <code>true</code> if the current file or folder is valid for this
     *         plate reader
     */
    public abstract boolean isValidPlate(File file);

    /**
     * @param folder
     *        the folder containing the well plate to load
     * @param progress
     *        the progress bar to indicate the loading status
     * @return the loaded well plate
     * @throws IOException
     *         If the files cannot be correctly read.
     */
    public abstract OldWellPlate loadPlateFromFolder(File folder, Optional<CancelableProgressFrame> progress)
            throws IOException;

    /**
     * Loads the specified field into the given sequence. The sequence should be
     * pre-initialized with the proper metadata to avoid having to reload generic
     * plate information for each field
     * 
     * @param field
     *        the field to load
     * @param sequence
     *        the sequence where the field should be loaded
     * @throws IOException
     *         If the field fails to be loaded from files.
     */
    public abstract void loadField(OldField field, Sequence sequence) throws IOException;
}
