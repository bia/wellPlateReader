package plugins.adufour.hcs.io;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.RectangularShape;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import javax.media.jai.RenderedImageAdapter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.drew.imaging.tiff.TiffMetadataReader;
import com.drew.imaging.tiff.TiffProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.google.common.util.concurrent.AtomicDouble;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.TIFFDecodeParam;
import com.sun.media.jai.codecimpl.TIFFCodec;

import icy.file.FileUtil;
import icy.gui.frame.progress.CancelableProgressFrame;
import icy.image.IcyBufferedImage;
import icy.image.colormap.IcyColorMap;
import icy.image.colormap.LinearColorMap;
import icy.sequence.MetaDataUtil;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.util.ColorUtil;
import icy.util.OMEUtil;
import icy.util.XMLUtil;
import ome.units.quantity.Length;
import ome.units.unit.Unit;
import ome.xml.meta.OMEXMLMetadata;
import ome.xml.model.primitives.PositiveInteger;
import plugins.adufour.hcs.data.OldField;
import plugins.adufour.hcs.data.OldWell;
import plugins.adufour.hcs.data.OldWellPlate;

/**
 * Specialized well plate reader for the Opera system (PerkinElmer)
 * 
 * @deprecated Use {@link WellPlateReader_EvotecPerkinElmerOperaFlex} instead.
 * @author Alexandre Dufour
 */
@Deprecated
public class OldWellPlateReader_Opera extends OldWellPlateReader
{
    @Override
    public String getSystemName()
    {
        return "Opera (PerkinElmer)";
    }

    private final static java.io.FileFilter FLEX_FILE_FILTER = new java.io.FileFilter()
    {
        @Override
        public boolean accept(File pathname)
        {
            return FileUtil.getFileExtension(pathname.getName(), false).equalsIgnoreCase("flex");
        }
    };

    private final static java.io.FileFilter COLUMBUS_FILE_FILTER = new java.io.FileFilter()
    {
        @Override
        public boolean accept(File pathname)
        {
            return pathname.getName().endsWith(".meax") || pathname.getName().endsWith(".ColumbusIDX.xml");
        }
    };

    public boolean isValidPlate(File folder)
    {
        boolean containsFlexFiles = FileUtil.getFiles(folder.getPath(), FLEX_FILE_FILTER, true, false,
                false).length > 0;
        boolean hasColombusIndexFile = FileUtil.getFiles(folder.getPath(), COLUMBUS_FILE_FILTER, true, false,
                false).length > 0;
        return containsFlexFiles && !hasColombusIndexFile;
    }

    private class LoadFileTask implements Callable<Void>
    {

        OldWellPlateReader_Opera readerReference;
        AtomicReference<OldWellPlate> wellPlate;
        OMEXMLMetadata metadata;
        Map<String, Element> filterCombinations;
        Map<String, Map<Integer, Point2D>> plateLayout;
        AtomicDouble fieldWidth, fieldHeight, pixelSizeX, pixelSizeY;
        AtomicBoolean firstRun;
        String filePath;

        public LoadFileTask(OldWellPlateReader_Opera reader, AtomicReference<OldWellPlate> wellPlateReference,
                OMEXMLMetadata metadata, Map<String, Element> filterCombinations,
                Map<String, Map<Integer, Point2D>> plateLayout, AtomicDouble fieldWidth, AtomicDouble fieldHeight,
                AtomicDouble pixelSizeX, AtomicDouble pixelSizeY, AtomicBoolean firstRun, String filePath)
        {
            this.wellPlate = wellPlateReference;
            this.readerReference = reader;
            this.metadata = metadata;
            this.filterCombinations = filterCombinations;
            this.plateLayout = plateLayout;
            this.fieldWidth = fieldWidth;
            this.fieldHeight = fieldHeight;
            this.pixelSizeX = pixelSizeX;
            this.pixelSizeY = pixelSizeY;
            this.firstRun = firstRun;
            this.filePath = filePath;
        }

        @Override
        public Void call() throws Exception
        {
            Metadata flexMetadata = TiffMetadataReader.readMetadata(new File(filePath));
            Thread.yield();
            Directory flexDirectory = flexMetadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            // 0xfeb0 is the Tag containing the metadata xml.
            String flexDescriptionXml = flexDirectory.getDescription(0xfeb0);

            try (FileOutputStream outputStream = new FileOutputStream(new File(filePath + "_toXML.xml")))
            {
                outputStream.write(flexDescriptionXml.getBytes());
            }

            Document flexDocument = XMLUtil.createDocument(flexDescriptionXml);

            Element rootElement = XMLUtil.getElement(flexDocument, "Root");
            Element flexElement = XMLUtil.getElement(rootElement, "FLEX");

            if (firstRun.get())
            {
                // Initialize the metadata using the first image
                String device = flexElement.getAttribute("OperaDevice");
                synchronized (metadata)
                {
                    metadata.setInstrumentID(device, 0);
                }

                // 1) Store filter combinations
                Optional<Element> filterCombinationsElement = Optional
                        .ofNullable(XMLUtil.getElement(flexElement, "FilterCombinations"));
                if (filterCombinationsElement.isPresent())
                {
                    for (Element filterCombinationElement : XMLUtil.getElements(filterCombinationsElement.get(),
                            "FilterCombination"))
                    {
                        // Store the combination as the images reference them (ExpXCamY)
                        String expID = XMLUtil.getAttributeValue(filterCombinationElement, "ID", "");
                        expID = "Exp" + expID.substring(expID.length() - 1);

                        for (Element sliderRefElement : XMLUtil.getElements(filterCombinationElement, "SliderRef"))
                        {
                            String camID = XMLUtil.getAttributeValue(sliderRefElement, "ID", "");
                            // FIXME Anything other than "Camera..." is not supported
                            if (!camID.startsWith("Camera"))
                                continue;

                            camID = "Cam" + camID.substring(camID.length() - 1);
                            synchronized (filterCombinations)
                            {
                                filterCombinations.put(expID + camID, sliderRefElement);
                            }
                        }
                    }
                }
                Thread.yield();
                // 2) Initialize the well plate

                Element plateElement = XMLUtil.getElement(flexElement, "Plate");
                String plateType = XMLUtil.getElementValue(plateElement, "PlateName", "Unknown plate");
                String plateID = XMLUtil.getElementValue(plateElement, "Barcode", "");
                synchronized (metadata)
                {
                    metadata.setPlateName(plateType, 0);
                    metadata.setPlateID(plateID, 0);
                }

                // Standard plate formats (rows * columns):
                // 6 : 2 * 3
                // 24 : 4 * 6
                // 96 : 8 * 12
                // 384 : 16 * 24
                // 1536: 32 * 48
                int nbRows = XMLUtil.getElementIntValue(plateElement, "XSize", 3);
                int nbCols = XMLUtil.getElementIntValue(plateElement, "YSize", 4);
                synchronized (metadata)
                {
                    metadata.setPlateRows(new PositiveInteger(nbRows), 0);
                    metadata.setPlateColumns(new PositiveInteger(nbCols), 0);
                }

                // Well shape and size
                Element wellShapeElement = XMLUtil.getElement(plateElement, "WellShape");
                String shape = XMLUtil.getElementValue(wellShapeElement, "Shape", "Circle");
                OldWell.Shape wellShape = OldWell.Shape.valueOf(shape);
                // NB: use microns instead of m to avoid display glitches (due to
                // round-off errors)
                // double wellWidth = XMLUtil.getElementDoubleValue(wellShapeElement, "XSize", 1d) * 1000000d;
                // double wellHeight = XMLUtil.getElementDoubleValue(wellShapeElement, "YSize", 1d) * 1000000d;

                // Create the well plate
                wellPlate.set(new OldWellPlate(readerReference, metadata, wellShape));

                // Read the (sub)layout(s)
                Optional<Element> sublayoutsElement = Optional
                        .ofNullable(XMLUtil.getElement(flexElement, "Sublayouts"));
                if (sublayoutsElement.isPresent())
                {
                    for (Element sublayoutElement : XMLUtil.getElements(sublayoutsElement.get(), "Sublayout"))
                    {
                        Map<Integer, Point2D> sublayout = new HashMap<Integer, Point2D>();
                        for (Element fieldElement : XMLUtil.getElements(sublayoutElement, "Field"))
                        {
                            int fieldID = XMLUtil.getAttributeIntValue(fieldElement, "No", 1);
                            // NB: use microns instead of meters to avoid display glitches
                            // (due to round-off errors)
                            double centerX = XMLUtil.getElementDoubleValue(fieldElement, "OffsetX", 0d) * 1000000d;
                            double centerY = XMLUtil.getElementDoubleValue(fieldElement, "OffsetY", 0d) * 1000000d;
                            sublayout.put(fieldID, new Point2D.Double(centerX, centerY));
                        }

                        String subLayoutID = XMLUtil.getAttributeValue(sublayoutElement, "ID", "");
                        Map<Integer, Point2D> layout;
                        synchronized (plateLayout)
                        {
                            layout = plateLayout.get(subLayoutID);
                            if (layout == null)
                            {
                                layout = new HashMap<>();
                                plateLayout.put(subLayoutID, layout);
                            }
                        }

                        Map<Integer, Point2D> layoutFinal = layout;
                        sublayout.forEach((key, value) -> {
                            synchronized (layoutFinal)
                            {
                                layoutFinal.put(key, value);
                            }
                        });

                    }
                }
            }
            Thread.yield();

            // Retrieve the image position w.r.t. the layout
            Element wellElement = XMLUtil.getElement(flexElement, "Well");
            Element wellCoordinateElement = XMLUtil.getElement(wellElement, "WellCoordinate");
            int row = XMLUtil.getAttributeIntValue(wellCoordinateElement, "Row", 0);
            int col = XMLUtil.getAttributeIntValue(wellCoordinateElement, "Col", 0);
            OldWell well = wellPlate.get().getWellAt(row - 1, col - 1);

            // Which sub-layout does this image belong to?
            String subLayoutID = XMLUtil.getElementValue(wellElement, "SublayoutRef", "");
            // Which field of the sub-layout?
            // (NB: this is stored _strangely_ at the single-image level)
            Element imagesElement = XMLUtil.getElement(wellElement, "Images");

            // Channels are stored as separate images
            ArrayList<Element> imageChannelsElement = XMLUtil.getElements(imagesElement, "Image");
            for (int currentChannel = 0; currentChannel < imageChannelsElement.size(); currentChannel++)
            {
                Thread.yield();
                Element imageElement = imageChannelsElement.get(currentChannel);

                if (firstRun.get()) // Read image information (size, channels)
                {
                    if (currentChannel == 0) // Read image size/resolution only once
                                             // (from the first channel)
                    {
                        // Pixel size (meters in FLEX, microns in OME)
                        pixelSizeX
                                .set(XMLUtil.getElementDoubleValue(imageElement, "ImageResolutionX", 1e-6d) * 1000000);
                        pixelSizeY
                                .set(XMLUtil.getElementDoubleValue(imageElement, "ImageResolutionY", 1e-6d) * 1000000);
                        synchronized (metadata)
                        {
                            MetaDataUtil.setPixelSizeX(metadata, 0, pixelSizeX.get());
                            MetaDataUtil.setPixelSizeY(metadata, 0, pixelSizeY.get());
                        }

                        // Image size (in pixels)
                        int imageWidth = XMLUtil.getElementIntValue(imageElement, "ImageWidth", 0);
                        int imageHeight = XMLUtil.getElementIntValue(imageElement, "ImageHeight", 0);

                        // Field size
                        fieldWidth.set(pixelSizeX.get() * imageWidth);
                        fieldHeight.set(pixelSizeY.get() * imageHeight);
                    }

                    // Read channel information (mostly wavelength)

                    // Filter => channel name
                    String cameraRef = XMLUtil.getElementValue(imageElement, "CameraRef", null);
                    Element filterCombinationElement = filterCombinations.get(cameraRef);

                    String filter = XMLUtil.getAttributeValue(filterCombinationElement, "Filter", null);
                    if (filter == null)
                    {
                        filter = "540/75"; // Default filter -> Can be changed for another combination
                    }
                    synchronized (metadata)
                    {
                        metadata.setChannelName(filter, 0, currentChannel);
                    }

                    // Excitation/Emmission
                    String[] filterValues = filter.split("/");
                    int lambdaEx = Integer.parseInt(filterValues[0]) - Integer.parseInt(filterValues[1]);
                    // Apply a very arbitrary Stoke shift
                    int lambdaEm = lambdaEx + 40;
                    Unit<Length> lambdaUnit = Unit.CreateBaseUnit("nanometers", "lambda");
                    Length excitation = new Length(lambdaEx, lambdaUnit);
                    Length emmission = new Length(lambdaEm, lambdaUnit);
                    synchronized (metadata)
                    {
                        metadata.setChannelExcitationWavelength(excitation, 0, currentChannel);
                        metadata.setChannelEmissionWavelength(emmission, 0, currentChannel);
                    }

                    // Channel color (use the emmission wavelength)
                    Color color = ColorUtil.getColorFromWavelength(lambdaEm);
                    synchronized (metadata)
                    {
                        metadata.setChannelColor(OMEUtil.getOMEColor(color), 0, currentChannel);
                    }

                }

                // Field ID, position and size
                int fieldNumber = XMLUtil.getElementIntValue(imageElement, "Sublayout", 0);
                Point2D fieldPosition = plateLayout.get(subLayoutID).get(fieldNumber);

                // NB: Opera stores this relative to the well / field center!
                RectangularShape wellShape = well.getShape().wellShape;
                double fieldX = wellShape.getCenterX() + (fieldPosition.getX() - 0.5d * fieldWidth.get());
                double fieldY = wellShape.getCenterY() + (fieldPosition.getY() - 0.5d * fieldHeight.get());

                // Store the image file and position in the well plate
                synchronized (well)
                {
                    well.addField(new OldField(well, fieldNumber, fieldX, fieldY, fieldWidth.get(), fieldHeight.get(),
                            new String[] {filePath}));
                }

            }

            firstRun.set(false);
            return null;
        }

    }

    @Override
    public OldWellPlate loadPlateFromFolder(File folder, Optional<CancelableProgressFrame> progress) throws IOException
    {
        String[] files = FileUtil.getFiles(folder.getPath(), FLEX_FILE_FILTER, true, false, false);
        if (files.length == 0)
            throw new IcyHandledException("Invalid folder: " + folder.getPath());
        /*
         * List<String> fileList = Arrays.stream(files).collect(Collectors.toList());
         * Collections.shuffle(fileList); files =
         * fileList.stream().toArray(String[]::new);
         */
        AtomicReference<OldWellPlate> wellPlate = new AtomicReference<OldWellPlate>();
        OMEXMLMetadata metadata = OMEUtil.createOMEXMLMetadata();
        metadata.setFolderName(folder.getPath(), 0);

        // Prepare generic stuff here:
        // 1) filters
        Map<String, Element> filterCombinations = Collections.synchronizedMap(new HashMap<String, Element>());
        // 2) plate layout: (sublayoutID -> (fieldID -> fieldPosition))
        Map<String, Map<Integer, Point2D>> plateLayout = Collections
                .synchronizedMap(new HashMap<String, Map<Integer, Point2D>>());
        // 3) Image/Field size/resolution
        AtomicDouble fieldWidth = new AtomicDouble(), fieldHeight = new AtomicDouble();
        AtomicDouble pixelSizeX = new AtomicDouble(), pixelSizeY = new AtomicDouble();

        int numProcessors = Runtime.getRuntime().availableProcessors();
        ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(numProcessors * 2);
        threadPool.prestartAllCoreThreads();
        ExecutorCompletionService<Void> completionService = new ExecutorCompletionService<>(threadPool);

        int processedFiles = 0;
        CancelableProgressFrame progressFrame = progress.orElseGet(null);
        if (progressFrame != null)
            progressFrame.setLength(files.length);

        AtomicBoolean firstRun = new AtomicBoolean(true);

        // Initial task to initialize data.
        completionService.submit(new LoadFileTask(this, wellPlate, metadata, filterCombinations, plateLayout,
                fieldWidth, fieldHeight, pixelSizeX, pixelSizeY, firstRun, files[0]));

        Future<Void> fileTask;
        try
        {
            fileTask = completionService.take();

            if (progressFrame != null)
            {
                if (progressFrame.isCancelRequested())
                    throw new IcyHandledException("Loading was cancelled (" + folder.getPath() + ")");
                progressFrame.setPosition(++processedFiles);
                progressFrame.setMessage(String.format("Loading plate %s (file %d/%d)...", folder.getName(),
                        processedFiles, files.length));
            }

            fileTask.get();

            // Fill with the rest of files.
            for (int fileIt = 1; fileIt < files.length; fileIt++)
            {
                String filePath = files[fileIt];
                completionService.submit(new LoadFileTask(this, wellPlate, metadata, filterCombinations, plateLayout,
                        fieldWidth, fieldHeight, pixelSizeX, pixelSizeY, firstRun, filePath));
            }
            threadPool.shutdown();

            for (int fileIt = 1; fileIt < files.length; fileIt++)
            {
                fileTask = completionService.take();
                if (progressFrame != null)
                {
                    if (progressFrame.isCancelRequested())
                    {
                        threadPool.shutdownNow();
                        boolean finished = threadPool.awaitTermination(5, TimeUnit.SECONDS);
                        if (!finished)
                        {
                            throw new IcyHandledException(
                                    "Loading was cancelled but hasn't stopped yet(" + folder.getPath() + ")");
                        }
                        throw new IcyHandledException("Loading was cancelled (" + folder.getPath() + ")");
                    }
                    progressFrame.setPosition(++processedFiles);
                    progressFrame.setMessage(String.format("Loading plate %s (file %d/%d)...", folder.getName(),
                            processedFiles, files.length));
                }
                Thread.yield();
                fileTask.get();
            }
        }
        catch (InterruptedException e)
        {
            threadPool.shutdownNow();
            e.printStackTrace();
            throw new IcyHandledException("Loading was cancelled (" + folder.getPath() + ")");
        }
        catch (ExecutionException e)
        {
            threadPool.shutdownNow();
            e.printStackTrace();
            throw new IcyHandledException(e);
        }

        if (progress.isPresent())
            progress.get().setLength(files.length);

        return wellPlate.get();
    }

    @Override
    public void loadField(OldField field, Sequence sequence) throws IOException
    {
        sequence.setName("Well " + field.getWell().getAlphanumericID() + ", Field #" + field.getID());
        // The field is stored in a single image
        String path = field.getFilePaths()[0];

        // Bypass BioFormats for faster access (!!)
        SeekableStream ss = SeekableStream.wrapInputStream(new FileInputStream(path), true);
        ImageDecoder dec = TIFFCodec.createImageDecoder("tiff", ss, new TIFFDecodeParam());

        Metadata mmd;
        try
        {
            mmd = TiffMetadataReader.readMetadata(new File(path));
        }
        catch (TiffProcessingException e)
        {
            throw new IOException(e);
        }
        Directory dir = mmd.getDirectories().iterator().next();
        // The first tag is the XML description of the entire plate
        // (sweet!!)
        Document flex;
        try
        {
            flex = XMLUtil.createDocument(dir.getTags().iterator().next().getDescription());
        }
        catch (SAXException e)
        {
            throw new IOException(e);
        }
        Element _root = XMLUtil.getElement(flex, "Root");
        Element _flex = XMLUtil.getElement(_root, "FLEX");

        // Store Z-stacks
        TreeMap<String, Element> stackRefs = new TreeMap<String, Element>();
        Element _stacks = XMLUtil.getElement(_flex, "Stacks");
        for (Element _stack : XMLUtil.getElements(_stacks, "Stack"))
            stackRefs.put(XMLUtil.getAttributeValue(_stack, "ID", ""), _stack);

        // Start reading the well contents
        Element _well = XMLUtil.getElement(_flex, "Well");
        Element _images = XMLUtil.getElement(_well, "Images");

        // Prepare the Z-stack
        Element _stack = stackRefs.get(XMLUtil.getElementValue(_well, "StackRef", null));
        int sizeZ = _stack.getChildNodes().getLength();

        OMEXMLMetadata metadata = sequence.getOMEXMLMetadata();
        sequence.beginUpdate();

        // For each slice, read and compile the different channels
        // NB: slice indexes start at 1
        for (int z = 1; z <= sizeZ; z++)
        {
            // Read the individual channels for the current slice
            ArrayList<IcyBufferedImage> channels = new ArrayList<IcyBufferedImage>();

            for (Element _image : XMLUtil.getElements(_images, "Image"))
            {
                int currentZ = XMLUtil.getElementIntValue(_image, "Stack", 0);
                if (z != currentZ)
                    continue;

                // Read the image data
                int tiffPage = XMLUtil.getAttributeIntValue(_image, "BufferNo", 0);
                RenderedImage _plane = dec.decodeAsRenderedImage(tiffPage);
                channels.add(IcyBufferedImage.createFrom(new RenderedImageAdapter(_plane)));
            }

            // Channels are ready to be merged
            IcyBufferedImage plane = IcyBufferedImage.createFrom(channels);

            // The plane can be added to the stack
            sequence.setImage(0, z - 1, plane); // FIXME handle time-lapse data
        }

        // Set field position (units are already in microns)
        sequence.setPositionX(field.getBounds().getX());
        sequence.setPositionY(field.getBounds().getY());

        // Set the color map (if available)
        try
        {
            for (int c = 0; c < sequence.getSizeC(); c++)
            {
                int emmission = metadata.getChannelEmissionWavelength(0, c).value().intValue();
                Color color = OMEUtil.getJavaColor(metadata.getChannelColor(0, c));
                IcyColorMap colorMap = new LinearColorMap(emmission + "nm", color);
                sequence.setColormap(c, colorMap, true);
            }
        }
        catch (Exception npe)
        {
            // forget it...
        }

        sequence.endUpdate();
    }

}
