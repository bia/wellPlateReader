package plugins.adufour.hcs.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IPlate;
import danyfel80.wells.data.IWell;
import danyfel80.wells.data.columbus.ColumbusChannel;
import danyfel80.wells.data.columbus.ColumbusField;
import danyfel80.wells.data.columbus.ColumbusImage;
import danyfel80.wells.data.columbus.ColumbusPlane;
import danyfel80.wells.data.columbus.ColumbusPlate;
import danyfel80.wells.data.columbus.ColumbusTimepoint;
import danyfel80.wells.util.MessageProgressListener;
import danyfel80.wells.util.stream.StreamUtils;
import icy.file.FileUtil;
import icy.image.IcyBufferedImage;
import icy.image.colormap.IcyColorMap;
import icy.image.colormap.LinearColorMap;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import icy.type.collection.array.ByteArrayConvert;
import icy.type.dimension.Dimension2D;
import icy.type.dimension.Dimension2D.Double;
import loci.formats.FormatException;
import loci.formats.in.MinimalTiffReader;
import ome.xml.meta.OMEXMLMetadata;
import ome.xml.model.primitives.PositiveInteger;

public class WellPlateReader_ColumbusOperaFlex extends AbstractWellPlateReader
{

    @Override
    public String getSystemName()
    {
        return "PerkinElmer Columbus Flex";
    }

    private final static java.io.FileFilter FLEX_FILE_FILTER = new java.io.FileFilter()
    {
        @Override
        public boolean accept(File pathname)
        {
            return FileUtil.getFileExtension(pathname.getName(), false).equalsIgnoreCase("flex");
        }
    };

    private final static java.io.FileFilter COLUMBUS_FILE_FILTER = new java.io.FileFilter()
    {
        @Override
        public boolean accept(File pathname)
        {
            return pathname.getName().endsWith(".meax") || pathname.getName().endsWith(".ColumbusIDX.xml");
        }
    };

    public boolean isValidPlate(File folder)
    {
        boolean containsFlexFiles = FileUtil.getFiles(folder.getPath(), FLEX_FILE_FILTER, true, false,
                false).length > 0;
        boolean hasColombusIndexFile = FileUtil.getFiles(folder.getPath(), COLUMBUS_FILE_FILTER, true, false,
                false).length > 0;
        return containsFlexFiles && hasColombusIndexFile;
    }

    @Override
    public Future<ColumbusPlate> loadPlateFromFolder(File folder, MessageProgressListener progressListener)
    {
        ExecutorService executor = Executors.newSingleThreadExecutor((Runnable r) -> {
            return new Thread(r, "ColumbusWellPlateReader");
        });

        Future<ColumbusPlate> future = executor.submit(() -> {
            return loadPlateFromFolder_internal(folder, progressListener);
        });

        executor.shutdown();
        return future;
    }

    private ColumbusPlate loadPlateFromFolder_internal(File folder, MessageProgressListener progressListener)
            throws IOException
    {
        String[] flexFiles = FileUtil.getFiles(folder.getPath(), FLEX_FILE_FILTER, true, false, false);
        if (flexFiles.length == 0)
            throw new IOException("Invalid folder: " + folder.getPath() + " does not contain any flex file.");

        String[] columbusFiles = FileUtil.getFiles(folder.getPath(), COLUMBUS_FILE_FILTER, true, false, false);
        if (flexFiles.length == 0)
            throw new IOException("Invalid folder: " + folder.getPath() + " does not contain any columbus xml file.");

        if (progressListener != null)
        {
            progressListener.notifyProgress(-1, "Loading well plate: " + columbusFiles[0]);
        }

        ColumbusPlate plate = new ColumbusPlate.Builder(columbusFiles[0]).progressListener(progressListener).build();

        if (progressListener != null)
        {
            progressListener.notifyProgress(1, "Well plate loaded: " + columbusFiles[0]);
        }
        return plate;
    }

    /**
     * @param field
     *        The field to load. The field must be of type {@link ColumbusField}.
     */
    @Override
    public Future<? extends Sequence> loadField(IPlate plate, IWell well, IField field, Sequence sequence,
            MessageProgressListener progressListener)
    {
        CompletableFuture<Sequence> future = new CompletableFuture<Sequence>();
        if (!(field instanceof ColumbusField))
        {
            future.completeExceptionally(
                    new ClassCastException("Provided field is not of type " + ColumbusField.class.getName()));
            return future;
        }

        if (!(plate instanceof ColumbusPlate))
        {
            future.completeExceptionally(
                    new ClassCastException("Provided field is not of type " + ColumbusField.class.getName()));
            return future;
        }

        if (sequence == null)
        {
            sequence = new Sequence();
        }
        sequence.beginUpdate();

        ColumbusPlate columbusPlate = (ColumbusPlate) plate;
        OMEXMLMetadata metadata = sequence.getOMEXMLMetadata();
        metadata.setPlateName(columbusPlate.getName(), 0);
        metadata.setPlateID(columbusPlate.getMeasurementId(), 0);
        metadata.setPlateRows(new PositiveInteger(columbusPlate.getDimension().height), 0);
        metadata.setPlateColumns(new PositiveInteger(columbusPlate.getDimension().width), 0);

        sequence.setPositionX(field.getPosition().getX());
        sequence.setPositionY(field.getPosition().getY());
        Dimension2D.Double pixelSize = new Dimension2D.Double();
        Map<Integer, String> channelNames = new HashMap<>();
        Map<Integer, IcyColorMap> channelColors = new HashMap<>();

        try
        {
            loadPlanes(columbusPlate, (ColumbusField) field, sequence, pixelSize, channelNames, channelColors);

            sequence.setPixelSizeX(pixelSize.getSizeX());
            sequence.setPixelSizeY(pixelSize.getSizeY());
            for (Entry<Integer, String> entry : channelNames.entrySet())
            {
                sequence.setChannelName(entry.getKey()-1, entry.getValue());
            }
            for (Entry<Integer, IcyColorMap> entry : channelColors.entrySet())
            {
                sequence.setColormap(entry.getKey()-1, entry.getValue(), true);
            }
        }
        finally
        {
            sequence.endUpdate();
        }
        future.complete(sequence);

        return future;
    }

    private void loadPlanes(ColumbusPlate plate, ColumbusField field, Sequence sequence, Double pixelSize,
            Map<Integer, String> channelNames, Map<Integer, IcyColorMap> channelColors)
    {
        List<List<IcyBufferedImage>> planes = field.getPlanes().values().stream()
                .sorted(Comparator.comparingDouble(ColumbusPlane::getPositionZ))
                .map(StreamUtils.wrapFunction(
                        plane -> loadPlane(sequence, pixelSize, channelNames, channelColors, plate, field, plane)))
                .collect(Collectors.toList());

        int z = 0, t;
        for (Iterator<List<IcyBufferedImage>> itZ = planes.iterator(); itZ.hasNext();)
        {
            List<IcyBufferedImage> planeImages = (List<IcyBufferedImage>) itZ.next();
            t = 0;
            for (Iterator<IcyBufferedImage> itT = planeImages.iterator(); itT.hasNext();)
            {
                IcyBufferedImage image = (IcyBufferedImage) itT.next();
                sequence.setImage(t, z, image);
                t++;
            }
            z++;
        }
    }

    private List<IcyBufferedImage> loadPlane(Sequence sequence, Dimension2D.Double pixelSize,
            Map<Integer, String> channelNames, Map<Integer, IcyColorMap> channelColors, ColumbusPlate plate,
            ColumbusField field, ColumbusPlane plane)
    {
        List<IcyBufferedImage> timeImages = plane.getTimepoints().values().stream()
                .sorted(Comparator.comparingLong(ColumbusTimepoint::getId))
                .map(StreamUtils.wrapFunction(
                        t -> loadTimePoint(sequence, pixelSize, channelNames, channelColors, plate, field, plane, t)))
                .collect(Collectors.toList());
        return timeImages;
    }

    private IcyBufferedImage loadTimePoint(Sequence sequence, Dimension2D.Double pixelSize,
            Map<Integer, String> channelNames, Map<Integer, IcyColorMap> channelColors, ColumbusPlate plate,
            ColumbusField field, ColumbusPlane plane, ColumbusTimepoint t)
    {
        List<IcyBufferedImage> channelImages = t.getChannels().values().stream()
                .sorted(Comparator.comparingLong(ColumbusChannel::getId))
                .map(StreamUtils.wrapFunction(ch -> loadChannel(sequence, pixelSize, channelNames, channelColors, plate,
                        field, plane, t, ch)))
                .collect(Collectors.toList());
        return IcyBufferedImage.createFrom(channelImages);
    }

    private IcyBufferedImage loadChannel(Sequence sequence, Dimension2D.Double pixelSize,
            Map<Integer, String> channelNames, Map<Integer, IcyColorMap> channelColors, ColumbusPlate plate,
            ColumbusField field, ColumbusPlane plane, ColumbusTimepoint t, ColumbusChannel ch) throws IOException
    {
        String folderPath = plate.getFolder();
        String flexFile = ch.getImage().getUrl();

        String fullFilePath = Paths.get(folderPath, flexFile).toString();

        ColumbusImage image = ch.getImage();

        channelNames.put((int) ch.getId(), ch.getName());
        IcyColorMap colorMap = new LinearColorMap(((int) ch.getEmissionWavelength()) + "nm", ch.getColor());
        channelColors.put((int) ch.getId(), colorMap);
        pixelSize.setSize(image.getResolutionX(), image.getResolutionY());
        MinimalTiffReader reader = new MinimalTiffReader();

        try
        {
            reader.setId(fullFilePath);
            return loadIcyBufferedImage(reader, (int) ch.getImage().getBufferNumber(), (int) ch.getImage().getSizeX(),
                    (int) ch.getImage().getSizeY());
        }
        catch (FormatException e)
        {
            throw new IOException(e);
        }
        finally
        {
            reader.close();
        }
    }

    /**
     * Loads the channel image from the reader at the given series with the given size.
     * 
     * @param reader
     *        Image reader
     * @param series
     *        The series to load
     * @param sizeX
     *        Size X
     * @param sizeY
     *        Size Y
     * @return The channel image.
     * @throws IOException
     *         If there is a problem reading the file.
     */
    private IcyBufferedImage loadIcyBufferedImage(MinimalTiffReader reader, int series, int sizeX, int sizeY)
            throws IOException
    {
        final DataType dataType = DataType.getDataTypeFromFormatToolsType(reader.getPixelType());
        final boolean little = reader.isLittleEndian();

        byte[] bytes;
        try
        {
            bytes = reader.openBytes(series);
        }
        catch (FormatException e)
        {
            throw new IOException(e);
        }

        Object pixelData = Array1DUtil.createArray(dataType, sizeX * sizeY);
        ByteArrayConvert.byteArrayTo(bytes, 0, pixelData, 0, bytes.length, little);

        return new IcyBufferedImage(sizeX, sizeY, pixelData, dataType.isSigned());
    }

}
