package plugins.adufour.hcs.io;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import icy.gui.frame.progress.CancelableProgressFrame;
import icy.image.IcyBufferedImage;
import icy.sequence.MetaDataUtil;
import icy.sequence.Sequence;
import icy.util.OMEUtil;
import icy.util.XMLUtil;
import ome.xml.meta.OMEXMLMetadata;
import ome.xml.model.primitives.PositiveInteger;
import plugins.adufour.hcs.data.OldField;
import plugins.adufour.hcs.data.OldWell;
import plugins.adufour.hcs.data.OldWell.Shape;
import plugins.adufour.hcs.data.OldWellPlate;

/**
 * Well plate reader for plates exported by the Image Mining software (yup, I
 * didn't forget you old friend...).
 * 
 * @deprecated Use {@link WellPlateReader_Im} instead.
 * @author Alexandre Dufour
 */
@Deprecated
public class OldWellPlateReader_IM extends OldWellPlateReader
{

    @Override
    public String getSystemName()
    {
        return "IM";
    }

    @Override
    public boolean isValidPlate(File folder)
    {
        return false;
        // if (!folder.exists() || !folder.isDirectory())
        // return false;
        //
        // // Check if an XML descriptor exists next to this directory
        // return new File(folder.getPath() + ".xml").exists();
    }

    @Override
    public OldWellPlate loadPlateFromFolder(File folder, Optional<CancelableProgressFrame> progress) throws IOException
    {
        OMEXMLMetadata metadata = OMEUtil.createOMEXMLMetadata();

        // Unfortunately not much is known about well plates exported by poor old
        // IM...

        Document descriptor = XMLUtil.loadDocument(folder.getPath() + ".xml");

        Element expGrid = XMLUtil.getElement(descriptor, "ExperimentGrid");

        Element plateSettings = XMLUtil.getElement(expGrid, "PlateSetting");

        metadata.setPlateID(XMLUtil.getAttributeValue(plateSettings, "GID", "unknown plate"), 0);
        int nbCols = XMLUtil.getAttributeIntValue(plateSettings, "XWells", 0);
        int nbRows = XMLUtil.getAttributeIntValue(plateSettings, "YWells", 0);
        metadata.setPlateRows(new PositiveInteger(nbRows), 0);
        metadata.setPlateColumns(new PositiveInteger(nbCols), 0);

        int channelCpt = 0;
        for (Element channel : XMLUtil.getElements(expGrid, "Wavelength"))
        {
            String chName = XMLUtil.getAttributeValue(channel, "Value", "ch " + channelCpt);
            metadata.setChannelName(chName, 0, channelCpt++);
        }

        Element xres = XMLUtil.getElement(expGrid, "XResolution");
        Element yres = XMLUtil.getElement(expGrid, "YResolution");
        Element zres = XMLUtil.getElement(expGrid, "ZResolution");

        MetaDataUtil.setPixelSizeX(metadata, 0, XMLUtil.getAttributeDoubleValue(xres, "Value", 1.0));
        MetaDataUtil.setPixelSizeY(metadata, 0, XMLUtil.getAttributeDoubleValue(yres, "Value", 1.0));
        MetaDataUtil.setPixelSizeZ(metadata, 0, XMLUtil.getAttributeDoubleValue(zres, "Value", 1.0));

        OldWellPlate wellPlate = new OldWellPlate(this, metadata, Shape.newCircle());

        for (Element experiment : XMLUtil.getElements(expGrid, "Experiment"))
        {
            int row = XMLUtil.getAttributeIntValue(experiment, "YPosition", 0);
            int col = XMLUtil.getAttributeIntValue(experiment, "XPosition", 0);
            int ID = XMLUtil.getAttributeIntValue(experiment, "PartIndex", 0);
            String path = XMLUtil.getAttributeValue(experiment, "Path", "");

            OldWell well = wellPlate.getWellAt(row, col);
            OldField field = new OldField(well, ID);
            field.addFilePath(folder.getPath() + File.separator + path);
            well.addField(field);
        }

        return wellPlate;
    }

    @Override
    public void loadField(OldField field, Sequence sequence) throws IOException
    {
        File file = new File(field.getFilePaths()[0]);
        if (!file.exists())
        {
            sequence.removeAllImages();
            return;
        }

        // Bypass BioFormats for faster access (!!)
        // Actually, Bioformats can't even read this!

        ImageReader reader = ImageIO.getImageReadersByFormatName("tiff").next();
        reader.setInput(new FileImageInputStream(file));

        int nChannels = reader.getNumImages(true);
        ArrayList<BufferedImage> channels = new ArrayList<>(nChannels);
        for (int c = 0; c < nChannels; c++)
        {
            ImageReadParam p = new ImageReadParam();
            p.setDestination(
                    new BufferedImage(reader.getWidth(c), reader.getHeight(c), BufferedImage.TYPE_USHORT_GRAY));
            channels.add(reader.read(c, p));
        }

        BufferedImage im = IcyBufferedImage.createFrom(channels);

        // The plane can be added to the stack
        sequence.setImage(0, 0, im);
    }

}
