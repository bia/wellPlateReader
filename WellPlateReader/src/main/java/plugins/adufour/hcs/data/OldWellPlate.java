package plugins.adufour.hcs.data;

import java.io.IOException;
import java.util.Iterator;

import org.xml.sax.SAXException;

import danyfel80.wells.data.IPlate;
import icy.sequence.Sequence;
import ome.xml.meta.OMEXMLMetadata;
import plugins.adufour.hcs.io.OldWellPlateReader;

/**
 * @deprecated Use {@link IPlate} instead.
 * @author Daniel Felipe Gonzalez Obando
 */
@Deprecated
public class OldWellPlate
{
    private final OldWellPlateReader wellPlateReader;

    private final OMEXMLMetadata metadata;

    private final Sequence sequence;

    /**
     * Linearized 2D array containing all wells
     */
    private final OldWell[] wells;

    /**
     * @param reader
     *        the reader that creates this well plate (will be used to load
     *        individual fields)
     * @param metadata
     *        the well plate metadata containing the plate layout
     * @param shape
     *        the shape of the wells in this plate
     */
    public OldWellPlate(OldWellPlateReader reader, OMEXMLMetadata metadata, OldWell.Shape shape)
    {
        this.wellPlateReader = reader;
        this.metadata = metadata;
        this.sequence = new Sequence(metadata);

        // Initialize the wells (even if they contain no image)
        int nbRows = getNbRows();
        int nbCols = getNbCols();
        wells = new OldWell[nbRows * nbCols];
        for (int row = 0; row < nbRows; row++)
            for (int col = 0; col < nbCols; col++)
                wells[row * nbCols + col] = new OldWell(shape, row, col);
    }

    /**
     * Builds a well plate considering the metadata and the array of wells (already
     * prepared before calling this constructor).
     * 
     * @param reader
     *        the reader that creates this well plate (will be used to load
     * @param metadata
     *        the well plate metadata containing the plate layout
     * @param wells
     *        The wells contained in the plate. Make sure to use the same
     *        size as in the metadata.
     * @throws IllegalArgumentException
     *         If the size of the wells array is not
     *         equivalent to the size specified in the
     *         metadata.
     */
    public OldWellPlate(OldWellPlateReader reader, OMEXMLMetadata metadata, OldWell[] wells)
    {
        this.wellPlateReader = reader;
        this.metadata = metadata;
        this.sequence = new Sequence(metadata);

        // Initialize the wells with parameter
        int nbRows = getNbRows();
        int nbCols = getNbCols();
        if (nbRows * nbCols == wells.length)
            this.wells = wells;
        else
            throw new IllegalArgumentException("The size of the wells array (" + wells.length
                    + ") is not compatible with the metadata (" + nbRows + ", " + nbCols + ")");
    }

    /**
     * @return The number of rows in this well plate
     */
    public int getNbRows()
    {
        return metadata.getPlateRows(0).getValue();
    }

    /**
     * @return The number of columns in this well plate
     */
    public int getNbCols()
    {
        return metadata.getPlateColumns(0).getValue();
    }

    public OldWell getWellAt(int row, int col)
    {
        return wells[row * getNbCols() + col];
    }

    public OMEXMLMetadata getMetaData()
    {
        return metadata;
    }

    @Override
    public String toString()
    {
        String plateID = metadata.getPlateID(0);
        String plateType = metadata.getPlateName(0);

        return plateID + " (" + plateType + ")";
    }

    public Sequence loadField(OldField field) throws IOException, SAXException
    {
        wellPlateReader.loadField(field, sequence);
        if (field != null)
            sequence.setName(field.getWell().getAlphanumericID());
        return sequence;
    }

    /**
     * @return A well iterator that automatically skips empty wells
     */
    public Iterator<OldWell> wellIterator()
    {
        return wellIterator(null);
    }

    /**
     * @return A well iterator that automatically skips empty wells
     * @param filters
     *        one or more templates (separated by spaces) used to select
     *        only a subset of wells, e.g.:
     *        <ul>
     *        <li><code>"A"</code> will select all wells in row A</li>
     *        <li><code>"A 03 B12"</code> will select all wells in row A,
     *        column 03, and well B12</li>
     *        <li>etc.</li>
     *        </ul>
     */
    public Iterator<OldWell> wellIterator(final String filters)
    {
        return new Iterator<OldWell>()
        {
            int currentWellIndex = -1;
            int nextWellIndex = 0;
            OldWell nextWell = null;

            @Override
            public OldWell next()
            {
                currentWellIndex = nextWellIndex;
                return nextWell;
            }

            @Override
            public boolean hasNext()
            {
                for (int newWell = currentWellIndex + 1; newWell < wells.length; newWell++)
                {
                    OldWell candidateWell = wells[newWell];

                    if (candidateWell.isEmpty())
                        continue;

                    // the well is automatically valid if there are no filters
                    boolean isValidWell = (filters == null);

                    if (filters != null)
                    {
                        for (String filter : filters.split("\\s+"))
                            if (candidateWell.getAlphanumericID().contains(filter))
                            {
                                isValidWell = true;
                                break;
                            }
                    }

                    if (isValidWell)
                    {
                        nextWell = wells[newWell];
                        nextWellIndex = newWell;
                        return true;
                    }
                }
                return false;
            }
        };
    }

    /**
     * @return A field iterator that automatically skips empty wells
     */
    public Iterator<OldField> fieldIterator()
    {
        return fieldIterator(null);
    }

    /**
     * @return A field iterator that automatically skips empty wells
     * @param templates
     *        one or more templates (separated by spaces) used to select
     *        only a subset of wells, e.g.:
     *        <ul>
     *        <li><code>"A"</code> will select all wells in row A</li>
     *        <li><code>"A 03 B12"</code> will select all wells in row A,
     *        column 03, and well B12</li>
     *        <li>etc.</li>
     *        </ul>
     */
    public Iterator<OldField> fieldIterator(final String templates)
    {
        return new Iterator<OldField>()
        {
            Iterator<OldWell> wellIterator = wellIterator(templates);
            Iterator<OldField> currentFieldIterator = null;

            @Override
            public OldField next()
            {
                return currentFieldIterator.next();
            }

            @Override
            public boolean hasNext()
            {
                // Are there more fields remaining?
                if (currentFieldIterator != null && currentFieldIterator.hasNext())
                    return true;

                // If not, a new well perhaps?
                if (wellIterator.hasNext())
                {
                    currentFieldIterator = wellIterator.next().fieldIterator();
                    return currentFieldIterator.hasNext();
                }

                return false;
            }
        };
    }
}