package plugins.adufour.hcs.data;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import danyfel80.wells.data.IWell;
import icy.type.dimension.Dimension2D;
import icy.util.StringUtil;

/**
 * @deprecated Use {@link IWell} instead.
 * @author Daniel Felipe Gonzalez Obando
 */
@Deprecated
public class OldWell
{
    public static class Shape
    {

        public static Shape newCircle()
        {
            return new Shape(new Ellipse2D.Double(0, 0, 1, 1));
        }

        public static Shape newSquare()
        {
            return new Shape(new Rectangle2D.Double(0, 0, 1, 1));
        }

        public static Shape newRectangle()
        {
            return new Shape(new Rectangle2D.Double(0, 0, 2, 1));
        }

        public final RectangularShape wellShape;

        private Shape(RectangularShape shape)
        {
            this.wellShape = shape;
        }

        public void setDimension(double width, double height)
        {
            wellShape.setFrame(0, 0, width, height);
        }

        public RectangularShape getShape()
        {
            return wellShape;
        }

        public Dimension2D getDimension()
        {
            return new Dimension2D.Double(wellShape.getWidth(), wellShape.getHeight());
        }

        public static Shape valueOf(String shape)
        {
            Shape s;
            switch (shape.toLowerCase())
            {
                case "circle":
                    s = newCircle();
                    break;
                case "square":
                    s = newSquare();
                    break;
                default: // rectangle
                    s = newRectangle();
                    break;
            }
            return s;
        }
    }

    /**
     * Well shape (used for display)
     */
    final Shape shape;

    final int row;

    final int col;

    /**
     * Map storing the field per ID
     */
    private final Map<Integer, OldField> fields = new LinkedHashMap<>();

    public OldWell(Shape shape, int row, int col)
    {
        this.row = row;
        this.col = col;
        this.shape = shape;
    }

    public String getAlphanumericID()
    {
        char alphaRow = 'A';
        alphaRow += row;
        return alphaRow + StringUtil.toString(col + 1, 2);
    }

    public void addField(OldField field)
    {
        fields.put(field.getID(), field);
    }

    public boolean isEmpty()
    {
        return fields.isEmpty();
    }

    public Shape getShape()
    {
        return shape;
    }

    public int getColumn()
    {
        return col;
    }

    public int getRow()
    {
        return row;
    }

    public OldField getField(int ID)
    {
        return fields.get(ID);
    }

    public Collection<OldField> getFields()
    {
        return fields.values();
    }

    public Iterator<OldField> fieldIterator()
    {
        return fields.values().iterator();
    }
}