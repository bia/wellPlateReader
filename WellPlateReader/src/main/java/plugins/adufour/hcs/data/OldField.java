package plugins.adufour.hcs.data;

import java.awt.geom.Rectangle2D;
import java.util.Arrays;

import danyfel80.wells.data.IField;

/**
 * @deprecated Use {@link IField} instead.
 * @author Daniel Felipe Gonzalez Obando
 */
@Deprecated
public class OldField
{
    private final OldWell well;

    private final int ID;

    private final Rectangle2D.Double bounds;

    private String[] filePaths;

    public OldField(OldWell well, int ID)
    {
        this(well, ID, Double.NaN, Double.NaN);
    }

    public OldField(OldWell well, int ID, double x, double y)
    {
        this(well, ID, x, y, Double.NaN, Double.NaN, new String[0]);
    }

    public OldField(OldWell well, int ID, double x, double y, double w, double h, String[] filePaths)
    {
        this.well = well;
        this.ID = ID;
        this.bounds = new Rectangle2D.Double(x, y, w, h);
        this.filePaths = filePaths;
    }

    public void addFilePath(String filePath)
    {
        int n = filePaths.length;
        filePaths = Arrays.copyOf(filePaths, n + 1);
        filePaths[n] = filePath;
    }

    public void setFieldSize(double w, double h)
    {
        bounds.width = w;
        bounds.height = h;
    }

    public int getID()
    {
        return ID;
    }

    public Rectangle2D getBounds()
    {
        return bounds;
    }

    public String[] getFilePaths()
    {
        return filePaths;
    }

    public OldWell getWell()
    {
        return well;
    }

    @Override
    public String toString()
    {
        return well.getAlphanumericID() + "#" + ID;
    }
}
