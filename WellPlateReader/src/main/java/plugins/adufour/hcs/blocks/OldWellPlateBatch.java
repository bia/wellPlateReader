package plugins.adufour.hcs.blocks;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import icy.gui.frame.progress.CancelableProgressFrame;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.system.SystemUtil;
import plugins.adufour.blocks.lang.Batch;
import plugins.adufour.blocks.lang.FileBatch;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.hcs.OldWellPlateImporter;
import plugins.adufour.hcs.data.OldField;
import plugins.adufour.hcs.data.OldWellPlate;
import plugins.adufour.hcs.io.OldWellPlateReader;
import plugins.adufour.vars.gui.FileMode;
import plugins.adufour.vars.gui.model.FileTypeModel;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarFile;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarString;
import plugins.adufour.vars.util.VarListener;

/**
 * A well plate batch is a work-flow that will process every image of every well
 * in a given well plate (or a selection thereof). Supported plate formats are
 * the same as those supported by the {@link OldWellPlateImporter} plug-in.
 * 
 * @deprecated Use {@link WellPlateBatch} instead.
 * @author Alexandre Dufour
 */
@Deprecated
public class OldWellPlateBatch extends Batch
{
    // INTERFACE //

    /**
     * The folder containing the well plate. Although we could have extended
     * {@link FileBatch}, we choose to extend just {@link Batch} since we don't
     * need extension filters or recursive sub-folder analysis
     */
    private VarFile plateFolder;

    private VarSequence element;

    private VarString wellFilter;

    private VarString plateID;

    private VarString wellID;

    private VarInteger fieldID;

    // LOCAL STUFF //

    private OldWellPlateReader reader;

    private OldWellPlate currentWellPlate;

    /**
     * Indicates whether the current well plate is loaded (avoids reloading it
     * every time the protocol is run)
     */
    private boolean wellPlateLoaded = false;

    private Iterator<OldField> fieldIterator;

    @Override
    public Var<?> getBatchSource()
    {
        if (plateFolder == null)
        {
            // Make sure the well plate is reloaded only when this parameter changes
            plateFolder = new VarFile("Plate folder", null, new VarListener<File>()
            {
                @Override
                public void valueChanged(Var<File> source, File oldValue, File newValue)
                {
                    wellPlateLoaded = false;
                }

                @Override
                public void referenceChanged(Var<File> source, Var<? extends File> oldReference,
                        Var<? extends File> newReference)
                {
                    wellPlateLoaded = false;
                }
            });
            plateFolder.setDefaultEditorModel(new FileTypeModel(null, FileMode.FOLDERS, null, false));
        }
        return plateFolder;
    }

    @Override
    public VarSequence getBatchElement()
    {
        if (element == null)
        {
            // WARNING: do *not* change the name of this variable
            // why? see declareInput() and VarList.add()
            element = new VarSequence("Sequence", null);
        }
        return element;
    }

    @Override
    public void initializeLoop()
    {
        if (!wellPlateLoaded) // Fetch a valid reader and load the well plate
        {
            File folder = plateFolder.getValue(true);
            reader = OldWellPlateImporter.getReaderFor(folder);
            if (reader == null)
                throw new IcyHandledException("No plate reader found for folder " + folder.getPath());

            @SuppressWarnings("unchecked")
            Optional<CancelableProgressFrame> progress = (Optional<CancelableProgressFrame>) (SystemUtil.isHeadLess()
                    ? Optional.empty()
                    : Optional.of(new CancelableProgressFrame("Loading well plate...")));

            try
            {
                currentWellPlate = reader.loadPlateFromFolder(folder, progress);
                plateID.setValue(currentWellPlate.getMetaData().getPlateID(0));
                wellPlateLoaded = true;
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            finally
            {
                if (progress.isPresent())
                    progress.get().close();
            }
        }

        // Prepare the iterator
        fieldIterator = currentWellPlate.fieldIterator(wellFilter.getValue());
    }

    @Override
    public void beforeIteration()
    {
        try
        {
            Sequence sequence = new Sequence(currentWellPlate.getMetaData());
            OldField currentField = fieldIterator.next();
            wellID.setValue(currentField.getWell().getAlphanumericID());
            fieldID.setValue(currentField.getID());
            reader.loadField(currentField, sequence);
            element.setValue(sequence);
            Thread.yield();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public boolean isStopConditionReached()
    {
        return !(fieldIterator.hasNext());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        super.declareInput(inputMap);

        inputMap.add("wellFilter", wellFilter = new VarString("Well filter", ""));
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        super.declareOutput(outputMap);

        plateID = new VarString("Plate", "--");
        plateID.setEnabled(false);
        outputMap.add("plateID", plateID);

        wellID = new VarString("Well", "--");
        wellID.setEnabled(false);
        outputMap.add("wellID", wellID);

        fieldID = new VarInteger("Field", 0);
        fieldID.setEnabled(false);
        outputMap.add("fieldID", fieldID);
    }

    @Override
    public void declareLoopVariables(List<Var<?>> loopVariables)
    {
        super.declareLoopVariables(loopVariables);
        loopVariables.add(wellFilter);
        loopVariables.add(plateID);
        loopVariables.add(wellID);
        loopVariables.add(fieldID);
    }
}
