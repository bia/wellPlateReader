package plugins.adufour.hcs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import icy.gui.frame.progress.CancelableProgressFrame;
import icy.main.Icy;
import icy.plugin.PluginDescriptor;
import icy.plugin.PluginLoader;
import icy.plugin.abstract_.PluginActionable;
import icy.system.IcyHandledException;
import icy.system.thread.ThreadUtil;
import plugins.adufour.hcs.data.OldWellPlate;
import plugins.adufour.hcs.gui.OldWellPlateViewer;
import plugins.adufour.hcs.io.OldWellPlateReader;
import plugins.adufour.hcs.io.OldWellPlateReader_Opera;
import plugins.adufour.hcs.io.WellPlateReader_ColumbusOperaFlex;

/**
 * @deprecated use {@link WellPlateImporter} instead.
 * @author Daniel Felipe Gonzalez Obando
 */
@Deprecated
public class OldWellPlateImporter extends PluginActionable
{
    private static final Set<OldWellPlateReader> availableReaders = new HashSet<OldWellPlateReader>();

    @SuppressWarnings("unchecked")
    private static void loadImporters()
    {
        availableReaders.clear();

        ArrayList<Class<OldWellPlateReader>> importers = new ArrayList<>();
        for (PluginDescriptor pd : PluginLoader.getPlugins(OldWellPlateReader.class))
        {
            importers.add((Class<OldWellPlateReader>) pd.getPluginClass());
        }
        if (importers.isEmpty())
        {
            // Probably debugging within Eclipse => add one (known) entry manually
            importers.add(
                    (Class<OldWellPlateReader>) OldWellPlateReader_Opera.class.asSubclass(OldWellPlateReader.class));
            importers.add((Class<OldWellPlateReader>) WellPlateReader_ColumbusOperaFlex.class
                    .asSubclass(OldWellPlateReader.class));
        }

        // Add available file filters
        for (Class<OldWellPlateReader> importerClass : importers)
            try
            {
                availableReaders.add(importerClass.newInstance());
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
    }

    @Override
    public void run()
    {
        loadImporters();

        String lastUsedDirectory = getPreferencesRoot().get("lastUsedDirectory", null);

        JFileChooser jfc = new JFileChooser(lastUsedDirectory);

        // Build a descriptor for the file filter
        String tmpFilterName = "Plate folders (";
        for (OldWellPlateReader reader : availableReaders)
            tmpFilterName += reader.getSystemName() + ", ";
        // Replace the trailing comma with a closing parenthesis
        final String filterName = tmpFilterName.substring(0, tmpFilterName.length() - 2) + ")";

        jfc.setFileFilter(new FileFilter()
        {
            @Override
            public String getDescription()
            {
                return filterName;
            }

            @Override
            public boolean accept(File f)
            {
                return f.isDirectory();
            }
        });
        jfc.setAcceptAllFileFilterUsed(false);
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int result = jfc.showOpenDialog(Icy.getMainInterface().getMainFrame());

        if (result == JFileChooser.APPROVE_OPTION)
            new Thread(() -> {
                try
                {
                    File selection = jfc.getSelectedFile();

                    CancelableProgressFrame loadingProgress = new CancelableProgressFrame(
                            "Loading plate " + selection.getName());

                    getPreferencesRoot().put("lastUsedDirectory", selection.getPath());

                    OldWellPlateReader importer = getReaderFor(selection);
                    OldWellPlate wellPlate = importer.loadPlateFromFolder(selection, Optional.of(loadingProgress));

                    loadingProgress.close();

                    ThreadUtil.invokeLater(() -> {
                        OldWellPlateViewer wellPlateViewer = new OldWellPlateViewer(wellPlate);
                        wellPlateViewer.pack();
                        wellPlateViewer.addToDesktopPane();
                        wellPlateViewer.setVisible(true);
                    });
                }
                catch (IOException ioE)
                {
                    throw new IcyHandledException(
                            "Cannot load folder " + jfc.getSelectedFile().getName() + "\nReason: " + ioE.getMessage());
                }
            }).start();
    }

    /**
     * @param path
     *        Path to check.
     * @return <code>true</code> if at least one reader accepts the given file
     */
    public static boolean isValid(File path)
    {
        loadImporters();

        for (OldWellPlateReader reader : availableReaders)
            if (reader.isValidPlate(path))
                return true;

        return false;
    }

    /**
     * Looks for an appropriate readed of the files contained in the provided path.
     * 
     * @param file
     *        Path of the folder containing the files to read.
     * @return The appropriate reader for the files.
     */
    public static OldWellPlateReader getReaderFor(File file)
    {
        for (OldWellPlateReader reader : availableReaders)
            if (reader.isValidPlate(file))
                return reader;
        return null;
    }
}
