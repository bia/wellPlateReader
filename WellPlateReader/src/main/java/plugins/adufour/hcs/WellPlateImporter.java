package plugins.adufour.hcs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Future;

import danyfel80.wells.data.IPlate;
import danyfel80.wells.ui.WellPlateViewer;
import icy.plugin.PluginDescriptor;
import icy.plugin.PluginLoader;
import icy.system.IcyHandledException;
import icy.system.thread.ThreadUtil;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarFolder;
import plugins.adufour.hcs.io.AbstractWellPlateReader;
import plugins.adufour.hcs.io.WellPlateReader_EvotecPerkinElmerOperaFlex;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class WellPlateImporter extends EzPlug implements EzStoppable
{
    private static final Set<AbstractWellPlateReader> availableReaders = new HashSet<>();

    public static AbstractWellPlateReader getReaderFor(File file)
    {
        loadAvailableReaders();

        for (AbstractWellPlateReader reader : availableReaders)
            if (reader.isValidPlate(file))
                return reader;

        return null;
    }

    private static void loadAvailableReaders()
    {
        if (availableReaders.isEmpty())
            reloadAvailableReaders();
    }

    @SuppressWarnings("unchecked")
    public static void reloadAvailableReaders()
    {
        availableReaders.clear();

        ArrayList<Class<? extends AbstractWellPlateReader>> importers = new ArrayList<>();
        for (PluginDescriptor pd : PluginLoader.getPlugins(AbstractWellPlateReader.class))
        {
            importers.add((Class<? extends AbstractWellPlateReader>) pd.getPluginClass());
        }

        if (importers.isEmpty())
        {
            // Probably debugging within Eclipse => add ONE (known) entry manually
            importers.add(WellPlateReader_EvotecPerkinElmerOperaFlex.class.asSubclass(AbstractWellPlateReader.class));
        }

        // Add available file filters
        for (Class<? extends AbstractWellPlateReader> importerClass : importers)
            try
            {
                availableReaders.add(importerClass.newInstance());
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
    }

    private EzVarFolder varInDirectory;

    @Override
    protected void initialize()
    {
        varInDirectory = new EzVarFolder("Well plate folder", null);
        addEzComponent(varInDirectory);
    }

    @Override
    protected void execute()
    {
        File targetDirectory = varInDirectory.getValue(true);

        if (!isHeadLess())
        {
            this.getUI().setProgressBarVisible(true);
        }
        notifyProgressUI(0.001, "Loading plate " + targetDirectory.getName());

        getPreferencesRoot().put("lastUsedDirectory", targetDirectory.getPath());
        try
        {
            AbstractWellPlateReader reader = getReaderFor(targetDirectory);
            Future<? extends IPlate> plateFuture = reader.loadPlateFromFolder(targetDirectory, this::notifyProgressUI);
            IPlate plate = plateFuture.get();
            ThreadUtil.invokeLater(() -> {
                WellPlateViewer wellPlateViewer = new WellPlateViewer(plate, reader);
                wellPlateViewer.pack();
                wellPlateViewer.addToDesktopPane();
                wellPlateViewer.setVisible(true);
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new IcyHandledException(e);
        }
        finally
        {
            if (!isHeadLess())
            {
                this.getUI().setProgressBarVisible(false);
            }
        }
    }

    private void notifyProgressUI(double progress, String message)
    {
        if (isHeadLess())
        {
            System.out.println(
                    "Loading well plate " + ((int) Math.round(progress * 100)) + "%: " + Objects.toString(message));
        }
        else
        {
            if (!Double.isNaN(progress))
            {
                getUI().setProgressBarValue(progress);
            }
            if (message != null)
            {
                getUI().setProgressBarMessage(message);
            }
        }
    }

    @Override
    public void clean()
    {
    }

}
