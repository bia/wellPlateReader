/**
 * 
 */
package danyfel80.wells.util;

/**
 * A progress listener handling progress values and messages.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public interface MessageProgressListener
{
    /**
     * Notifies the progression to this listener.
     * 
     * @param progress
     *        The current progress value of completion. The value should be between 0 and 1. A value of -1 indicates that the progress status is indeterminate.
     *        A
     *        {@link Double#NaN} value indicates no value change is to be taken into account.
     * @param message
     *        The current message associated to the progress. If the value is {@code null}, no change is to be taken into account.
     */
    public void notifyProgress(double progress, String message);
}
