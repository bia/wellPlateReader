package danyfel80.wells.util;

public class MeasureUtil {

	public static double getLengthInMicrons(double length, String sourceUnit) {
		switch (sourceUnit) {
		case "m":
			return length * 1e6d;
		case "dm":
			return length * 1e5d;
		case "cm":
			return length * 1e4d;
		case "mm":
			return length * 1e3d;
		case "nm":
			return length * 1e-3d;
		default:
			return length;
		}
	}

}
