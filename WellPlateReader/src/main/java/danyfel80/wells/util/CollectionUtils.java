package danyfel80.wells.util;

import java.util.Map;

public class CollectionUtils {
	public static <K, V> Map.Entry<K, V> newEntry(K key, V value) {
		return new Map.Entry<K, V>() {
			@Override
			public K getKey() {
				return key;
			}

			@Override
			public V getValue() {
				// TODO Auto-generated method stub
				return value;
			}

			@Override
			public V setValue(V value) {
				throw new UnsupportedOperationException("no value modifications are allowed");
			}
		};
	}

	public static <T> Map.Entry<T, T> newPair(T v1, T v2) {
		return newEntry(v1, v2);
	}
}
