package danyfel80.wells.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IWell;

public class WellViewer extends JComponent implements MouseListener
{
    private static final long serialVersionUID = 2478991498042362510L;

    private IWell well;
    private Rectangle2D wellBounds;

    /**
     * Indicates whether the well is selected and should be highlighted in the
     * grid view
     */
    private boolean selected;

    private boolean showFields;

    private int selectedFieldId = 1;

    /**
     * The transform to the well's upper left hand corner (unit: pixels)
     */
    private final AffineTransform transform = new AffineTransform();

    public WellViewer(IWell well, boolean showFields)
    {
        this.well = well;
        if (well != null)
        {
            this.wellBounds = this.well.getShape().getShape().getBounds2D();
        }
        this.showFields = showFields;
        addMouseListener(this);

        // if the well has fields inside it, select the first one
        if (well != null && !well.getFields().isEmpty())
            setSelectedField(well.getFields().entrySet().stream().findFirst().get().getValue());
    }

    @Override
    protected void processMouseEvent(MouseEvent e)
    {
        if (well == null)
        {
            e.consume();
            return;
        }

        // Restrict mouse events to the well shape
        try
        {
            Point2D clickLocation = transform.inverseTransform(e.getPoint(), null);
            if (well.getShape().getShape().contains(clickLocation))
                super.processMouseEvent(e);
        }
        catch (NoninvertibleTransformException e1)
        {
            e1.printStackTrace();
        }
    }

    /**
     * Called when a field is clicked inside this well. The default implementation
     * is empty and should be overridden to handle such events
     * 
     * @param theWell
     *        the well in which a field has been clicked
     * @param theField
     *        the field that has been clicked
     */
    protected void fieldChanged(IWell theWell, IField theField)
    {

    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        if (well == null)
            return;

        Graphics2D g2 = (Graphics2D) g.create();

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setStroke(new BasicStroke(0.6f / (float) Math.log1p(transform.getScaleX())));
        g2.transform(transform);
        g2.setColor(Color.gray);
        Shape shape = well.getShape().getShape();

        g2.draw(shape);
        if (!well.getFields().isEmpty())
            g2.fill(shape);

        // Draw fields inside the well?
        if (showFields)
        {
            for (IField field : well.getFields().values())
            {
                g2.setColor(field.getId() == selectedFieldId ? Color.yellow : Color.darkGray);
                Rectangle2D fieldRectangle = well.getFieldBoundsOnWell(field);
                g2.fill(fieldRectangle);
            }
        }
        else if (selected)
        {
            g2.setStroke(new BasicStroke(2f / (float) Math.log1p(transform.getScaleX())));
            g2.setColor(Color.yellow);
            g2.draw(shape);
        }

        g2.dispose();
    }

    @Override
    public void setBounds(int x, int y, int w, int h)
    {
        if (well != null)
        {
            // Adjust the local transform
            transform.setToIdentity();
            int offset = Math.min(w, h);
            transform.translate((w - offset) / 2 + (0.05 * offset), (h - offset) / 2 + 0.05 * offset);
            double scaleX = 0.9 * offset / wellBounds.getWidth();
            double scaleY = 0.9 * offset / wellBounds.getHeight();
            if (scaleX > 0 && scaleY > 0)
                transform.scale(scaleX, scaleY);
            transform.translate(-wellBounds.getX(), -wellBounds.getY());
        }

        super.setBounds(x, y, w, h);
    }

    public void setWell(IWell well)
    {
        this.well = well;

        if (well == null || well.getFields().isEmpty())
        {
            this.wellBounds = null;
            repaint();
        }
        else
        {
            this.wellBounds = this.well.getShape().getShape().getBounds2D();
            setSelectedField(well.getFields().entrySet().stream().findFirst().get().getValue());
        }
    }

    public IWell getWell()
    {
        return well;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public int getSelectedFieldId()
    {
        return selectedFieldId;
    }

    public void setSelectedField(IField selectedField)
    {
        if (selectedField != null)
        {
            this.selectedFieldId = (int) selectedField.getId();
            fieldChanged(well, selectedField);
            repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        try
        {
            Point2D clickLocation = transform.inverseTransform(e.getPoint(), null);
            // Check if a field was clicked
            if (showFields && e.getClickCount() == 1)
            {
                Rectangle2D fieldBoundsOnWell;
                for (IField field : well.getFields().values())
                {
                    fieldBoundsOnWell = well.getFieldBoundsOnWell(field);
                    if (fieldBoundsOnWell.contains(clickLocation))
                    {
                        setSelectedField(field);
                        break;
                    }
                }
            }
        }
        catch (NoninvertibleTransformException nitE)
        {
        }
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
    }
}