package danyfel80.wells.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IPlate;
import danyfel80.wells.data.IWell;
import icy.gui.frame.IcyFrame;
import icy.gui.viewer.Viewer;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.util.StringUtil;
import plugins.adufour.hcs.io.AbstractWellPlateReader;

public class WellPlateViewer extends IcyFrame
{
    public WellPlateViewer(final IPlate wellPlate, AbstractWellPlateReader plateReader)
    {
        super(wellPlate.toString(), false, true, false, true, true);

        //////////////////////
        // GENERAL LAYOUT: //
        // a b c d | __ //
        // 1 o o o o | / \ //
        // 2 o o o o | \__/ //
        // 3 o o o o | //
        //////////////////////

        setLayout(new BorderLayout());

        ////////////////////////
        // CENTER: PLATE VIEW //
        ////////////////////////

        Dimension plateDimension = wellPlate.getDimension();
        final JPanel plateView = new JPanel(new GridLayout(plateDimension.height + 1, plateDimension.width + 1, 2, 2));
        plateView.setPreferredSize(new Dimension(250, 425));
        add(plateView, BorderLayout.CENTER);

        ////////////////////////////
        // EAST: SINGLE WELL VIEW //
        ////////////////////////////

        JPanel singleWellPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        singleWellPanel.setPreferredSize(new Dimension(300, 425));

        // SELECTED WELL (JLabel)
        // WELL (WellViewer)
        // SELECTED FIELD (JLabel)
        // FIELD SLIDER (JSlider)

        final JLabel selectedWellLabel = new JLabel("No well selected");
        final JLabel selectedFieldLabel = new JLabel("");
        final JSlider fieldSlider = new JSlider(JSlider.HORIZONTAL);
        fieldSlider.setVisible(false);
        fieldSlider.setBorder(new EmptyBorder(5, 10, 5, 15));

        Sequence viewerSequence = new Sequence();

        final WellViewer selectedWellViewer = new WellViewer(null, true)
        {
            private static final long serialVersionUID = -7385257944014813154L;

            @Override
            protected void fieldChanged(IWell well, IField field)
            {
                try
                {
                    Future<? extends Sequence> sequenceFuture = plateReader.loadField(wellPlate, well, field,
                            viewerSequence, null);
                    Sequence sequence = sequenceFuture.get();
                    sequence.setName("Well" + ((char) ('A' + well.getPositionInPlate().x - 1))
                            + StringUtil.toString(well.getPositionInPlate().y, 2) + "_Field" + field.getId());
                    selectedFieldLabel.setText("Field " + field.getId());
                    fieldSlider.setValue((int) field.getId());
                    if (sequence.getFirstViewer() == null)
                        new Viewer(sequence);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    throw new IcyHandledException("Cannot load well " + well.getId(), e);
                }
            }
        };
        selectedWellViewer.setPreferredSize(new Dimension(220, 220));

        fieldSlider.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                IWell well = selectedWellViewer.getWell();
                if (well == null || well.getFields().isEmpty())
                    return;

                int sliderValue = fieldSlider.getValue();
                if (sliderValue != selectedWellViewer.getSelectedFieldId())
                {
                    selectedWellViewer.setSelectedField(well.getFields().get((long) sliderValue));
                }
            }
        });

        singleWellPanel.add(selectedWellLabel);
        singleWellPanel.add(selectedWellViewer);
        singleWellPanel.add(selectedFieldLabel);
        singleWellPanel.add(fieldSlider);
        add(singleWellPanel, BorderLayout.EAST);

        /////////////////
        // PLATE LOGIC //
        /////////////////

        int nbRows = plateDimension.height + 1;
        int nbCols = plateDimension.width + 1;
        Map<Point, IWell> wellGrid = createWellGrid(wellPlate);
        // Header row
        for (int col = 0; col < nbCols; col++)
        {
            JLabel header = new JLabel(col == 0 ? "" : "" + ((char) ('A' + (col - 1))), JLabel.CENTER);
            header.setFont(header.getFont().deriveFont(header.getFont().getSize2D() - 2f));
            plateView.add(header);
        }
        for (int row = 1; row < nbRows; row++)
            for (int col = 0; col < nbCols; col++)
                if (col == 0)
                {
                    JLabel header = new JLabel(StringUtil.toString(row, 2), JLabel.CENTER);
                    header.setFont(header.getFont().deriveFont(header.getFont().getSize2D() - 2f));
                    plateView.add(header);
                }
                else
                {
                    final IWell well = wellGrid.get(new Point(col, row));
                    WellViewer wellViewInPlate = new WellViewer(well, false);
                    if (well != null)
                    {
                        final char colWell = (char) ('A' + (col - 1));
                        final int rowWell = row;

                        final List<Long> fieldIds = new ArrayList<>(well.getFields().keySet());
                        wellViewInPlate.addMouseListener(new MouseAdapter()
                        {
                            @Override
                            public void mouseClicked(MouseEvent e)
                            {
                                String wellId = "Well " + colWell + StringUtil.toString(rowWell, 2);
                                selectedWellLabel.setText(wellId);
                                selectedWellViewer.setWell(well);
                                fieldSlider.setVisible(!well.getFields().isEmpty());
                                if (!well.getFields().isEmpty())
                                {
                                    fieldSlider.setMinimum(1);
                                    fieldSlider.setMaximum(fieldIds.size());
                                }

                                // Highlight this well in the grid
                                for (Component component : plateView.getComponents())
                                {
                                    if (component instanceof WellViewer)
                                    {
                                        WellViewer wellViewer = (WellViewer) component;
                                        Long id = wellViewer.getWell() == null ? null : wellViewer.getWell().getId();
                                        wellViewer.setSelected(id != null && id.longValue() == well.getId());
                                    }
                                }
                                plateView.repaint();
                            }
                        });
                    }
                    plateView.add(wellViewInPlate);
                }
        repaint();
    }

    private Map<Point, IWell> createWellGrid(IPlate wellPlate)
    {
        return wellPlate.getWells().values().stream()
                .collect(Collectors.toMap(IWell::getPositionInPlate, Function.identity()));
    }
}