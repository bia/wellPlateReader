package danyfel80.wells.data;

import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.Map;

public interface IWell
{
    long getId();

    Point getPositionInPlate();

    Map<Long, ? extends IField> getFields();

    IWellShape getShape();

    /**
     * @param field
     *        The target field;
     * @return The bounds of the field relative to well bounds.
     */
    Rectangle2D getFieldBoundsOnWell(IField field);
}
