package danyfel80.wells.data.im;

import java.awt.Color;
import java.awt.Point;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageReader;

import org.w3c.dom.Element;

import danyfel80.wells.data.IChannel;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class ImChannel implements IChannel
{

    public static class Builder
    {

        public static ImChannel createChannel(Element gridElement, Path imagePath, Point positionInWell,
                ImageReader reader, int chId) throws IOException
        {
            ImChannel channel = new ImChannel();
            channel.id = chId;
            channel.name = "" + chId;

            channel.image = ImImage.Builder.createImage(gridElement, imagePath, positionInWell, reader, chId);
            return channel;
        }

    }

    private long id;
    private String name;
    private ImImage image;

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public Color getColor()
    {
        return null;
    }

    @Override
    public double getExcitationWavelength()
    {
        return 0;
    }

    @Override
    public double getEmissionWavelength()
    {
        return 0;
    }

    @Override
    public ImImage getImage()
    {
        return image;
    }

}
