package danyfel80.wells.data.im;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.geom.Dimension2D;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;

import javax.imageio.ImageReader;

import org.w3c.dom.Element;

import danyfel80.wells.data.IImage;
import icy.util.XMLUtil;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class ImImage implements IImage
{

    public static class Builder
    {

        public static ImImage createImage(Element gridElement, Path imagePath, Point positionInWell, ImageReader reader,
                int chId) throws IOException
        {
            ImImage image = new ImImage();
            image.id = chId;
            image.url = imagePath.toString();

            Element xres = XMLUtil.getElement(gridElement, "XResolution");
            Element yres = XMLUtil.getElement(gridElement, "YResolution");

            image.resolution = new icy.type.dimension.Dimension2D.Double(
                    XMLUtil.getAttributeDoubleValue(xres, "Value", 1.0),
                    XMLUtil.getAttributeDoubleValue(yres, "Value", 1.0));

            image.sizeXY = new Dimension(reader.getWidth(chId), reader.getHeight(chId));

            return image;
        }

    }

    private int id;
    private String url;
    private Dimension2D resolution;
    private Dimension sizeXY;

    @Override
    public String getVersion()
    {
        return null;
    }

    @Override
    public String getId()
    {
        return "" + id;
    }

    @Override
    public String getState()
    {
        return null;
    }

    @Override
    public long getBufferNumber()
    {
        return id;
    }

    @Override
    public String getUrl()
    {
        return url;
    }

    @Override
    public long getRow()
    {
        return 0;
    }

    @Override
    public long getColumn()
    {
        return 0;
    }

    @Override
    public long getFieldId()
    {
        return 0;
    }

    @Override
    public long getPlaneId()
    {
        return 0;
    }

    @Override
    public long getTimepointId()
    {
        return 0;
    }

    @Override
    public long getChannelId()
    {
        return id;
    }

    @Override
    public Color getChannelColor()
    {
        return null;
    }

    @Override
    public String getChannelType()
    {
        return null;
    }

    @Override
    public String getAcquisitionType()
    {
        return null;
    }

    public Dimension2D getResolution()
    {
        return resolution;
    }

    @Override
    public double getResolutionX()
    {
        return resolution.getWidth();
    }

    @Override
    public double getResolutionY()
    {
        return resolution.getHeight();
    }

    public Dimension getSizeXY()
    {
        return sizeXY;
    }

    @Override
    public long getSizeX()
    {
        return sizeXY.width;
    }

    @Override
    public long getSizeY()
    {
        return sizeXY.height;
    }

    @Override
    public double getPositionX()
    {
        return 0;
    }

    @Override
    public double getPositionY()
    {
        return 0;
    }

    @Override
    public double getAbsPositionZ()
    {
        return 0;
    }

    @Override
    public double getPositionZ()
    {
        return 0;
    }

    @Override
    public double getTime()
    {
        return 0;
    }

    @Override
    public Date getDate()
    {
        return null;
    }

    @Override
    public double getExcitationWavelength()
    {
        return 0;
    }

    @Override
    public double getEmissionWavelength()
    {
        return 0;
    }

}
