/**
 * 
 */
package danyfel80.wells.data.im;

import java.awt.Shape;
import java.awt.geom.Rectangle2D;

import danyfel80.wells.data.IWellShape;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class ImWellShape implements IWellShape
{

    public static final ImWellShape DEFAULT_SHAPE;

    static
    {
        DEFAULT_SHAPE = new ImWellShape();
        DEFAULT_SHAPE.shape = new Rectangle2D.Double(0, 0, 1, 1);
    }

    private Shape shape;

    @Override
    public Shape getShape()
    {
        return shape;
    }

}
