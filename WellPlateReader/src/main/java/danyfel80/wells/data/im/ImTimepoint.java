package danyfel80.wells.data.im;

import java.awt.Point;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;

import org.w3c.dom.Element;

import danyfel80.wells.data.ITimepoint;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class ImTimepoint implements ITimepoint
{
    public static class Builder
    {
        public static ImTimepoint createSingleTimepoint(Element gridElement, Path imagePath, Point positionInWell)
                throws IOException
        {
            ImTimepoint timepoint = new ImTimepoint();
            timepoint.id = 0;

            if (!Files.exists(imagePath))
            {
                throw new IOException("image file does not exist: " + imagePath);
            }
            ImageReader reader = ImageIO.getImageReadersByFormatName("tiff").next();
            try
            {
                reader.setInput(new FileImageInputStream(imagePath.toFile()));

                timepoint.channels = new HashMap<>();
                int numChannels = reader.getNumImages(true);
                for (int c = 0; c < numChannels; c++)
                {
                    ImChannel channel = ImChannel.Builder.createChannel(gridElement, imagePath, positionInWell, reader,
                            c);
                    timepoint.channels.put((long) c, channel);
                }
            }
            finally
            {
                reader.dispose();
            }
            return timepoint;
        }
    }

    private long id;
    private Map<Long, ImChannel> channels;

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public Map<Long, ImChannel> getChannels()
    {
        return channels;
    }

}
