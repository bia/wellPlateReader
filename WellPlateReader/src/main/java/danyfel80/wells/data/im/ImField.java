package danyfel80.wells.data.im;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Element;

import danyfel80.wells.data.IField;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class ImField implements IField
{

    public Double position;

    public static class Builder
    {
        public static ImField createSingleField(Element gridElement, Path imagePath, Point positionInWell)
                throws IOException
        {
            ImField field = new ImField();
            field.id = 0;
            field.planes = new HashMap<>();
            ImPlane plane = ImPlane.Builder.createSinglePlane(gridElement, imagePath, positionInWell);
            field.planes.put(plane.getId(), plane);

            Dimension2D pixelResolution = new icy.type.dimension.Dimension2D.Double();
            Dimension imageSize = new Dimension();

            plane.getTimepoints().values().stream().findFirst()
                    .ifPresent(t -> t.getChannels().values().stream().findFirst().ifPresent(ch -> {
                        pixelResolution.setSize(ch.getImage().getResolution());
                        imageSize.setSize(ch.getImage().getSizeXY());
                    }));

            field.positionInWell = new Point2D.Double();
            field.bounds = new Rectangle2D.Double(0, 0, imageSize.width * pixelResolution.getWidth(),
                    imageSize.height * pixelResolution.getHeight());
            field.pixelBounds = new Rectangle2D.Double(0, 0, imageSize.width, imageSize.height);

            return field;
        }
    }

    private long id;
    private Point2D positionInWell;
    private Map<Long, ImPlane> planes;
    private Rectangle2D bounds;
    private Rectangle2D pixelBounds;

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public Point2D getPosition()
    {
        return positionInWell;
    }

    @Override
    public Map<Long, ImPlane> getPlanes()
    {
        return planes;
    }

    @Override
    public Rectangle2D getBounds()
    {
        return bounds;
    }

    @Override
    public Rectangle2D getPixelBounds()
    {
        return pixelBounds;
    }

}
