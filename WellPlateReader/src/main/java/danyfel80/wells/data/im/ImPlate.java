package danyfel80.wells.data.im;

import java.awt.Dimension;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.DoubleSupplier;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import danyfel80.wells.data.IPlate;
import danyfel80.wells.util.MessageProgressListener;
import danyfel80.wells.util.stream.StreamUtils;
import icy.util.XMLUtil;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class ImPlate implements IPlate
{

    /**
     * @author Daniel Felipe Gonzalez Obando
     */
    public static class Builder
    {

        private String xmlFile;
        private String folderPath;
        private MessageProgressListener progressListener;

        /**
         * @param xmlFilePath
         * @param folderPath 
         */
        public Builder(String xmlFilePath, String folderPath)
        {
            this.xmlFile = xmlFilePath;
            this.folderPath = folderPath;
        }

        public Builder progressListener(MessageProgressListener progressListener)
        {
            this.progressListener = progressListener;
            return this;
        }

        public ImPlate build() throws IOException
        {
            notifyProgress(() -> 0.01, () -> "Loading xml descriptor file");
            Document plateDescriptionDocument = loadXMLDescriptorFile();
            Element gridElement = XMLUtil.getElement(plateDescriptionDocument, "ExperimentGrid");
            Element plateSettings = XMLUtil.getElement(gridElement, "PlateSetting");

            ImPlate plate = new ImPlate();
            plate.id = XMLUtil.getAttributeValue(plateSettings, "GID", "unknown plate");
            plate.name = plate.id;
            plate.type = "Unknown";
            plate.dimension = new Dimension(XMLUtil.getAttributeIntValue(plateSettings, "XWells", 0),
                    XMLUtil.getAttributeIntValue(plateSettings, "YWells", 0));

            plate.wells = getWellsFromGridElement(plate, gridElement);

            return plate;
        }

        private Document loadXMLDescriptorFile() throws IOException
        {
            return Optional.ofNullable(XMLUtil.loadDocument(xmlFile))
                    .orElseThrow(() -> new IOException("Could not read xml from file"));
        }

        private Map<Long, ImWell> getWellsFromGridElement(ImPlate plate, Element gridElement)
        {
            List<Element> wellElements = XMLUtil.getElements(gridElement, "Experiment");
            Map<Long, ImWell> wells = wellElements.stream()
                    .map(StreamUtils.<Element, ImWell> wrapFunction(
                            wellElement -> ImWell.Builder.fromXMLElement(gridElement, wellElement, folderPath)))
                    .collect(Collectors.toMap(ImWell::getId, Function.identity()));
            return wells;
        }

        private void notifyProgress(DoubleSupplier progress, Supplier<String> message)
        {
            if (progressListener != null)
            {
                progressListener.notifyProgress(progress.getAsDouble(), message.get());
            }
        }

    }

    private String id;
    private String name;
    private String type;
    private Dimension dimension;
    private Map<Long, ImWell> wells;

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public String getType()
    {
        return type;
    }

    @Override
    public Dimension getDimension()
    {
        return dimension;
    }

    @Override
    public Map<Long, ImWell> getWells()
    {
        return wells;
    }

}
