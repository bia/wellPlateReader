package danyfel80.wells.data.im;

import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Element;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IWell;
import icy.util.XMLUtil;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class ImWell implements IWell
{
    public static class Builder
    {

        public static ImWell fromXMLElement(Element gridElement, Element wellElement, String platePath)
                throws IOException
        {
            int id = XMLUtil.getAttributeIntValue(wellElement, "PartIndex", 0);
            Point pos = new Point(XMLUtil.getAttributeIntValue(wellElement, "XPosition", 0),
                    XMLUtil.getAttributeIntValue(wellElement, "YPosition", 0));
            Path imagePath = Paths.get(platePath, XMLUtil.getAttributeValue(wellElement, "Path", ""));
            ImField field = ImField.Builder.createSingleField(gridElement, imagePath, pos);

            ImWell well = new ImWell();
            well.id = id;
            well.positionInPlate = pos;
            well.fields = new HashMap<>();
            well.fields.put(field.getId(), field);
            well.wellShape = ImWellShape.DEFAULT_SHAPE;

            return null;
        }

    }

    private long id;
    private Point positionInPlate;
    private Map<Long, ImField> fields;
    private ImWellShape wellShape;

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public Point getPositionInPlate()
    {
        return positionInPlate;
    }

    @Override
    public Map<Long, ImField> getFields()
    {
        return fields;
    }

    @Override
    public ImWellShape getShape()
    {
        return wellShape;
    }

    @Override
    public Rectangle2D getFieldBoundsOnWell(IField field)
    {
        return field.getBounds();
    }

}
