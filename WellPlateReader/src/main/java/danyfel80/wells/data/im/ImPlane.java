package danyfel80.wells.data.im;

import java.awt.Point;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Element;

import danyfel80.wells.data.IPlane;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class ImPlane implements IPlane
{

    public static class Builder
    {
        public static ImPlane createSinglePlane(Element gridElement, Path imagePath, Point positionInWell)
                throws IOException
        {
            ImPlane plane = new ImPlane();
            plane.id = 0;
            plane.positionZ = 0;
            plane.timepoints = new HashMap<>();
            ImTimepoint timepoint = ImTimepoint.Builder.createSingleTimepoint(gridElement, imagePath, positionInWell);
            plane.timepoints.put(timepoint.getId(), timepoint);
            return plane;
        }
    }

    private long id;
    private double positionZ;
    private Map<Long, ImTimepoint> timepoints;

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public double getPositionZ()
    {
        return positionZ;
    }

    @Override
    public Map<Long, ImTimepoint> getTimepoints()
    {
        return timepoints;
    }

}
