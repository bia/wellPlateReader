package danyfel80.wells.data;

import java.util.Map;

public interface ITimepoint {
	long getId();

	public Map<Long, ? extends IChannel> getChannels();
}
