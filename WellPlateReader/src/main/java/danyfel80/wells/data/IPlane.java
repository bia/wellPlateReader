package danyfel80.wells.data;

import java.util.Map;

public interface IPlane {
	long getId();

	double getPositionZ();

	public Map<Long, ? extends ITimepoint> getTimepoints();
}
