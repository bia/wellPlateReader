package danyfel80.wells.data;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Map;

public interface IField
{
    long getId();

    Point2D getPosition();

    Map<Long, ? extends IPlane> getPlanes();

    Rectangle2D getBounds();

    Rectangle2D getPixelBounds();
}
