package danyfel80.wells.data;

import java.awt.Shape;

/**
 * Represents the shape a well has when laid over the GUI.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public interface IWellShape
{
    Shape getShape();
}
