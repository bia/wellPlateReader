package danyfel80.wells.data;

import java.awt.Dimension;
import java.util.Map;

/**
 * A well plate.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public interface IPlate
{
    /**
     * @return The identifier of this plate.
     */
    String getId();

    /**
     * @return The name of this plate.
     */
    String getName();

    /**
     * @return The type of this plate.
     */
    String getType();

    /**
     * @return The dimension of this plate (number of rows and columns).
     */
    Dimension getDimension();

    /**
     * @return The wells contained in this plate, indexed by well id.
     */
    Map<Long, ? extends IWell> getWells();
}
