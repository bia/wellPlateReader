package danyfel80.wells.data.columbus;

import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.w3c.dom.Element;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IWell;
import danyfel80.wells.util.CollectionUtils;
import icy.util.XMLUtil;

public class ColumbusWell implements IWell
{

    public static class Builder
    {
        public static ColumbusWell fromXMLElement(Element wellElement, Map<String, ColumbusImage> images)
                throws RuntimeException
        {
            long wellId = XMLUtil.getElementLongValue(wellElement, "id", 0);
            int wellRow = XMLUtil.getElementIntValue(wellElement, "Row", 0);
            int wellCol = XMLUtil.getElementIntValue(wellElement, "Col", 0);
            Set<String> wellImageIds = XMLUtil.getElements(wellElement, "Image").stream()
                    .map(imageElement -> XMLUtil.getAttributeValue(imageElement, "id", null))
                    .collect(Collectors.toSet());

            ColumbusWell well = new ColumbusWell();
            well.id = wellId;
            well.positionInPlate = new Point(wellRow, wellCol);

            Map<String, ColumbusImage> wellImages = wellImageIds.stream().map(imId -> images.get(imId))
                    .collect(Collectors.toMap(ColumbusImage::getId, Function.identity()));

            Map<Long, List<ColumbusImage>> fieldImages = wellImages.values().stream()
                    .collect(Collectors.groupingBy(ColumbusImage::getFieldId));

            Map<Long, ColumbusField> fields = fieldImages.entrySet().stream()
                    .map(fieldEntry -> CollectionUtils.newEntry(fieldEntry.getKey(),
                            ColumbusField.Builder.fromImages(fieldEntry.getValue())))
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
            well.fields = fields;

            Rectangle2D wellRect = fields.values().stream().map(f -> f.getBounds()).reduce(new Rectangle2D.Double(),
                    (acc, elem) -> {
                        if (acc.isEmpty())
                            acc.setRect(elem);
                        else if (!elem.isEmpty())
                            acc.add(elem);
                        return acc;
                    });
            well.shape = new ColumbusWellShape(wellRect);

            return well;
        }
    }

    private long id;
    private Point positionInPlate;
    private Map<Long, ColumbusField> fields;
    private ColumbusWellShape shape;

    private ColumbusWell()
    {
    }

    public long getId()
    {
        return id;
    }

    @Override
    public Map<Long, ColumbusField> getFields()
    {
        return Collections.unmodifiableMap(fields);
    }

    @Override
    public Point getPositionInPlate()
    {
        return positionInPlate;
    }

    @Override
    public ColumbusWellShape getShape()
    {
        return shape;
    }

    @Override
    public Rectangle2D getFieldBoundsOnWell(IField field)
    {
        // creating a copy of the already field on well bounds.
        return field.getBounds().getBounds2D();
    }

    @Override
    public String toString()
    {
        String wellString = "Well " + Objects.toString(id) + " at (";
        wellString += (positionInPlate != null) ? (positionInPlate.x + ", " + positionInPlate.y) : "null";
        wellString += ")";
        return wellString;
    }
}
