package danyfel80.wells.data.columbus;

import java.awt.Dimension;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.DoubleSupplier;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.xml.bind.DatatypeConverter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import danyfel80.wells.data.IPlate;
import danyfel80.wells.util.MessageProgressListener;
import danyfel80.wells.util.stream.StreamUtils;
import icy.util.XMLUtil;

public class ColumbusPlate implements IPlate
{

    public static class Builder
    {
        private final static Date DEFAULT_DATE = new Date();

        private String xmlDescriptorFile;
        private MessageProgressListener progressListener;

        public Builder(String xmlDescriptorFile)
        {
            this.xmlDescriptorFile = xmlDescriptorFile;
            this.progressListener = null;
        }

        public Builder progressListener(MessageProgressListener progressListener)
        {
            this.progressListener = progressListener;
            return this;
        }

        public ColumbusPlate build() throws IOException
        {
            notifyProgress(() -> 0.01, () -> "Loading xml descriptor file");
            Document plateDescriptionDocument = loadXMLDescriptorFile();

            Element rootElement = XMLUtil.getRootElement(plateDescriptionDocument);

            notifyProgress(() -> 0.1, () -> "Creating plate element");
            Element platesElement = XMLUtil.getElement(rootElement, "Plates");
            List<Element> plateElements = XMLUtil.getElements(platesElement, "Plate");
            ColumbusPlate plate = getPlatefromElement(plateElements.get(0));
            plate.folder = Paths.get(xmlDescriptorFile).getParent().toString();
            
            notifyProgress(() -> 0.2, () -> "Reading plate images");
            Element imagesElement = XMLUtil.getElement(rootElement, "Images");
            Map<String, ColumbusImage> images = getImagesFromElement(imagesElement);

            notifyProgress(() -> 0.6, () -> "Reading plate wells");
            Element wellsElement = XMLUtil.getElement(rootElement, "Wells");
            plate.wells = getWellsFromElement(wellsElement, images, plate.wellIds);

            return plate;
        }

        private Document loadXMLDescriptorFile() throws IOException
        {
            return Optional.ofNullable(XMLUtil.loadDocument(xmlDescriptorFile))
                    .orElseThrow(() -> new IOException("Could not read xml from file"));
        }

        private ColumbusPlate getPlatefromElement(Element plateElement) throws RuntimeException
        {
            String plateId = XMLUtil.getElementValue(plateElement, "PlateID", null);
            String plateMeasurementId = XMLUtil.getElementValue(plateElement, "MeasurementID", null);
            Date plateDate = Optional.ofNullable(XMLUtil.getElementValue(plateElement, "MeasuremenetStartTime", null))
                    .flatMap(StreamUtils
                            .wrapFunction(timeStr -> Optional.of(DatatypeConverter.parseDateTime(timeStr).getTime())))
                    .orElse(DEFAULT_DATE);
            String plateName = XMLUtil.getElementValue(plateElement, "Name", plateId);
            String plateType = XMLUtil.getElementValue(plateElement, "PlateType", "");
            int plateRows = XMLUtil.getElementIntValue(plateElement, "PlateRows", 0);
            int plateColumns = XMLUtil.getElementIntValue(plateElement, "PlateColumns", 0);
            Set<Long> plateWellIds = XMLUtil.getElements(plateElement, "Well").stream()
                    .map(StreamUtils.wrapFunction(wellElement -> XMLUtil.getAttributeLongValue(wellElement, "id", -1L)))
                    .collect(Collectors.toSet());

            ColumbusPlate plate = new ColumbusPlate();
            plate.id = plateId;
            plate.measurementId = plateMeasurementId;
            plate.measurementDate = plateDate;
            plate.name = plateName;
            plate.type = plateType;
            plate.dimension = new Dimension(plateRows, plateColumns);
            plate.wellIds = plateWellIds;
            return plate;
        }

        private Map<String, ColumbusImage> getImagesFromElement(Element imagesElement)
        {
            List<Element> imageElements = XMLUtil.getElements(imagesElement, "Image");

            final int imageCount = imageElements.size();
            AtomicInteger readImages = new AtomicInteger();
            Map<String, ColumbusImage> images = imageElements.stream().map(StreamUtils.wrapFunction(e -> {
                int imageNumber = readImages.incrementAndGet();
                notifyProgress(() -> 0.2 + 0.4 * imageNumber / (double) imageCount,
                        () -> "Reading plate images (" + imageNumber + "/" + imageCount + ")");
                return e;
            })).map(StreamUtils.wrapFunction(imageElement -> ColumbusImage.Builder.fromXMLElement(imageElement)))
                    .collect(Collectors.toMap(ColumbusImage::getId, Function.identity()));
            return images;
        }

        private Map<Long, ColumbusWell> getWellsFromElement(Element wellsElement, Map<String, ColumbusImage> images,
                Set<Long> wellIds)
        {
            List<Element> wellElements = XMLUtil.getElements(wellsElement, "Well");

            final int wellsCount = wellElements.size();
            AtomicInteger readWells = new AtomicInteger();
            Map<Long, ColumbusWell> wells = wellElements.stream().map(StreamUtils.wrapFunction(e -> {
                int wellNumber = readWells.incrementAndGet();
                notifyProgress(() -> 0.6 + 0.4 * wellNumber / (double) wellsCount,
                        () -> "Reading plate wells (" + wellNumber + "/" + wellsCount + ")");
                return e;
            })).map(StreamUtils.wrapFunction(wellElement -> ColumbusWell.Builder.fromXMLElement(wellElement, images)))
                    .filter(well -> wellIds.contains(well.getId()))
                    .collect(Collectors.toMap(ColumbusWell::getId, Function.identity()));
            return wells;
        }

        private void notifyProgress(DoubleSupplier progress, Supplier<String> message)
        {
            if (progressListener != null)
            {
                progressListener.notifyProgress(progress.getAsDouble(), message.get());
            }
        }
    }

    private String folder;
    private String id;
    private String measurementId;
    private Date measurementDate;
    private String name;
    private String type;
    private Dimension dimension;
    private Set<Long> wellIds;
    private Map<Long, ColumbusWell> wells;

    private ColumbusPlate()
    {
    }
    
    public String getFolder() {
        return folder;
    }

    public String getId()
    {
        return id;
    }

    public String getMeasurementId()
    {
        return measurementId;
    }

    public Date getMeasurementDate()
    {
        return measurementDate;
    }

    public String getName()
    {
        return name;
    }

    public String getType()
    {
        return type;
    }

    public Set<Long> getWellIds()
    {
        return Collections.unmodifiableSet(wells != null ? wells.keySet() : wellIds);
    }

    @Override
    public Dimension getDimension()
    {
        return dimension;
    }

    @Override
    public Map<Long, ColumbusWell> getWells()
    {
        return wells;
    }

    @Override
    public String toString()
    {
        String plateID = getId();
        String plateType = getName();

        return plateID + " (" + plateType + ")";
    }

}
