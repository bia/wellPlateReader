package danyfel80.wells.data.columbus;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import danyfel80.wells.data.IPlane;
import danyfel80.wells.util.CollectionUtils;

public class ColumbusPlane implements IPlane
{

    public static final class Builder
    {

        public static ColumbusPlane fromImages(List<ColumbusImage> planeImages)
        {
            Map<Long, List<ColumbusImage>> timepointImages = planeImages.stream()
                    .collect(Collectors.groupingBy(ColumbusImage::getTimepointId));
            ColumbusPlane plane = new ColumbusPlane();
            plane.id = planeImages.stream().findAny().map(im -> im.getPlaneId()).orElse(-1L);
            plane.positionZ = planeImages.stream().findAny().map(im -> im.getPositionZ()).orElse(0d);
            plane.timepoints = timepointImages.entrySet().stream()
                    .map(timepointEntry -> CollectionUtils.newEntry(timepointEntry.getKey(),
                            ColumbusTimepoint.Builder.fromImages(timepointEntry.getValue())))
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
            return plane;
        }

    }

    private long id;
    private double positionZ;
    private Map<Long, ColumbusTimepoint> timepoints;

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public double getPositionZ()
    {
        return positionZ;
    }

    @Override
    public Map<Long, ColumbusTimepoint> getTimepoints()
    {
        return Collections.unmodifiableMap(timepoints);
    }

    @Override
    public String toString()
    {
        return "Plane " + getId() + ": posZ=" + getPositionZ();
    }

}
