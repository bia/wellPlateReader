package danyfel80.wells.data.columbus;

import java.awt.Color;

import danyfel80.wells.data.IChannel;

public class ColumbusChannel implements IChannel
{

    public static class Builder
    {
        public static ColumbusChannel fromImage(ColumbusImage image)
        {
            ColumbusChannel ch = new ColumbusChannel();
            ch.image = image;
            return ch;
        }
    }

    private ColumbusImage image;

    @Override
    public long getId()
    {
        return image.getChannelId();
    }

    @Override
    public String getName()
    {
        return image.getChannelName();
    }

    @Override
    public Color getColor()
    {
        return image.getChannelColor();
    }

    @Override
    public double getExcitationWavelength()
    {
        return image.getExcitationWavelength();
    }

    @Override
    public double getEmissionWavelength()
    {
        return image.getEmissionWavelength();
    }

    @Override
    public ColumbusImage getImage()
    {
        return image;
    }

    @Override
    public String toString()
    {
        return "Channel " + getId() + ": " + getColor().toString();
    }

}
