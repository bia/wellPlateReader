package danyfel80.wells.data.columbus;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import danyfel80.wells.data.IField;
import danyfel80.wells.util.CollectionUtils;

public class ColumbusField implements IField
{

    public static class Builder
    {

        public static ColumbusField fromImages(List<ColumbusImage> fieldImages)
        {
            Map<Long, List<ColumbusImage>> planeImages = fieldImages.stream()
                    .collect(Collectors.groupingBy(ColumbusImage::getPlaneId));
            ColumbusField field = new ColumbusField();
            field.id = fieldImages.stream().findAny().map(im -> im.getFieldId()).orElse(-1L);
            field.position = fieldImages.stream().findAny()
                    .map(im -> new Point2D.Double(im.getPositionX(), im.getPositionY())).orElse(new Point2D.Double());
            field.planes = planeImages.entrySet().stream()
                    .map(planeEntry -> CollectionUtils.newEntry(planeEntry.getKey(),
                            ColumbusPlane.Builder.fromImages(planeEntry.getValue())))
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
            field.bounds = fieldImages.stream().findAny()
                    .map(im -> new Rectangle2D.Double(im.getPositionX(), im.getPositionY(),
                            im.getSizeX() * im.getResolutionX(), im.getSizeY() * im.getResolutionY()))
                    .orElse(new Rectangle2D.Double());
            field.pixelBounds = fieldImages.stream().findAny()
                    .map(im -> new Rectangle2D.Double(im.getPositionX() / im.getResolutionX(),
                            im.getPositionY() / im.getResolutionY(), im.getSizeX(), im.getSizeY()))
                    .orElse(new Rectangle2D.Double());
            return field;
        }

    }

    private long id;
    public Point2D.Double position;
    private Map<Long, ColumbusPlane> planes;
    public Rectangle2D bounds;
    public Rectangle2D pixelBounds;

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public Point2D getPosition()
    {
        return position;
    }

    @Override
    public Map<Long, ColumbusPlane> getPlanes()
    {
        return Collections.unmodifiableMap(planes);
    }

    @Override
    public Rectangle2D getBounds()
    {
        return bounds;
    }

    @Override
    public Rectangle2D getPixelBounds()
    {
        return pixelBounds;
    }

    @Override
    public String toString()
    {
        String fieldString = "Field " + getId() + "[";
        if (getBounds() != null)
        {
            Rectangle2D bnds = getBounds();
            fieldString += bnds.getX() + ", " + bnds.getY() + ", " + bnds.getWidth() + ", " + bnds.getHeight();
        }
        else
        {
            fieldString += "null";
        }
        fieldString += "]";

        return fieldString;
    }

}
