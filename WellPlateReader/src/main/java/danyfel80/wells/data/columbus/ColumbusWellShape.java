/**
 * 
 */
package danyfel80.wells.data.columbus;

import java.awt.geom.Rectangle2D;

import danyfel80.wells.data.IWellShape;

/**
 * A Rectangular well shape for columbus plates
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class ColumbusWellShape implements IWellShape
{
    private Rectangle2D rect;

    /**
     * Creates an instance with the well rectangle in micrometers.
     * 
     * @param rectangle
     *        Well bounds.
     */
    public ColumbusWellShape(Rectangle2D rectangle)
    {
        this.rect = rectangle;
    }

    @Override
    public Rectangle2D getShape()
    {
        return rect;
    }

}
