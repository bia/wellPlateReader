package danyfel80.wells.data.columbus;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import danyfel80.wells.data.ITimepoint;

public class ColumbusTimepoint implements ITimepoint
{

    public static final class Builder
    {

        public static ColumbusTimepoint fromImages(List<ColumbusImage> timepointImages)
        {
            Map<Long, ColumbusChannel> channels = timepointImages.stream()
                    .map(im -> ColumbusChannel.Builder.fromImage(im))
                    .collect(Collectors.toMap(ColumbusChannel::getId, Function.identity()));

            ColumbusTimepoint timepoint = new ColumbusTimepoint();
            timepoint.id = timepointImages.stream().findAny().map(im -> im.getTimepointId()).orElse(-1L);
            timepoint.channels = channels;
            return timepoint;
        }

    }

    private long id;
    private Map<Long, ColumbusChannel> channels;

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public Map<Long, ColumbusChannel> getChannels()
    {
        return Collections.unmodifiableMap(channels);
    }

    @Override
    public String toString()
    {
        return "Timepoint " + getId();
    }
}
