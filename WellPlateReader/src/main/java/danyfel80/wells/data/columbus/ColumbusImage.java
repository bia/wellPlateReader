package danyfel80.wells.data.columbus;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.Date;
import java.util.Optional;

import javax.xml.bind.DatatypeConverter;

import org.w3c.dom.Element;

import danyfel80.wells.data.IImage;
import icy.util.ColorUtil;
import icy.util.XMLUtil;

public class ColumbusImage implements IImage
{

    public static class Builder
    {

        private final static Date DEFAULT_DATE = new Date();

        public static ColumbusImage fromXMLElement(Element imageElement)
        {
            String imageVersion = XMLUtil.getAttributeValue(imageElement, "Version", "1");
            String imageId = XMLUtil.getElementValue(imageElement, "id", null);
            String imageState = XMLUtil.getElementValue(imageElement, "State", "Ok");
            Element urlElement = XMLUtil.getElement(imageElement, "URL");
            long imageBufferNumber = XMLUtil.getAttributeLongValue(urlElement, "BufferNo", 0);
            String imageURL = urlElement.getTextContent();
            long imageRow = XMLUtil.getElementLongValue(imageElement, "Row", 0);
            long imageColumn = XMLUtil.getElementLongValue(imageElement, "Col", 0);
            long imageFieldId = XMLUtil.getElementLongValue(imageElement, "FieldID", 0);
            long imagePlaneId = XMLUtil.getElementLongValue(imageElement, "PlaneID", 0);
            long imageTimepointId = XMLUtil.getElementLongValue(imageElement, "TimepointID", 0);
            long imageChannelId = XMLUtil.getElementLongValue(imageElement, "ChannelID", 0);

            String imageChannelName = XMLUtil.getElementValue(imageElement, "ChannelName", "Channel " + imageChannelId);
            // Long imageColorValue = new Long(XMLUtil.getElementLongValue(imageElement, "ChannelColor", 0));
            // int blue = (int) ((imageColorValue >> 24) & 0xff);
            // int green = (int) ((imageColorValue >> 16) & 0xff);
            // int red = (int) ((imageColorValue >> 8) & 0xff);
            // int alpha = (int) (imageColorValue & 0xff);

            String imageChannelType = XMLUtil.getElementValue(imageElement, "ChannelType", "Fluorescence");
            String imageAcquisitionType = XMLUtil.getElementValue(imageElement, "AcquisitionType",
                    "SpinningDiskConfocal");

            Element resolutionXElement = XMLUtil.getElement(imageElement, "ImageResolutionX");
            double imageResolutionX = Double.parseDouble(resolutionXElement.getTextContent())
                    * (XMLUtil.getAttributeValue(resolutionXElement, "Unit", "m").equals("m") ? 1000000 : 1);
            Element resolutionYElement = XMLUtil.getElement(imageElement, "ImageResolutionY");
            double imageResolutionY = Double.parseDouble(resolutionYElement.getTextContent())
                    * (XMLUtil.getAttributeValue(resolutionYElement, "Unit", "m").equals("m") ? 1000000 : 1);

            long imageSizeX = XMLUtil.getElementLongValue(imageElement, "ImageSizeX", 0);
            long imageSizeY = XMLUtil.getElementLongValue(imageElement, "ImageSizeY", 0);

            Element positionXElement = XMLUtil.getElement(imageElement, "PositionX");
            double imagePositionX = Double.parseDouble(positionXElement.getTextContent())
                    * (XMLUtil.getAttributeValue(positionXElement, "Unit", "m").equals("m") ? 1000000 : 1);
            Element positionYElement = XMLUtil.getElement(imageElement, "PositionY");
            double imagePositionY = Double.parseDouble(positionYElement.getTextContent())
                    * (XMLUtil.getAttributeValue(positionYElement, "Unit", "m").equals("m") ? 1000000 : 1);
            Element positionZElement = XMLUtil.getElement(imageElement, "PositionZ");
            double imagePositionZ = Double.parseDouble(positionZElement.getTextContent())
                    * (XMLUtil.getAttributeValue(positionZElement, "Unit", "m").equals("m") ? 1000000 : 1);
            Element absPositionZElement = XMLUtil.getElement(imageElement, "AbsPositionZ");
            double imageAbsPositionZ = Double.parseDouble(absPositionZElement.getTextContent())
                    * (XMLUtil.getAttributeValue(absPositionZElement, "Unit", "m").equals("m") ? 1000000 : 1);

            Element timeElement = XMLUtil.getElement(imageElement, "MeasurementTimeOffset");
            double imageTime = Double.parseDouble(timeElement.getTextContent());

            Date imageDate = Optional.ofNullable(XMLUtil.getElementValue(imageElement, "AbsTime", null))
                    .flatMap(timeStr -> {
                        try
                        {
                            return Optional.of(DatatypeConverter.parseDateTime(timeStr).getTime());
                        }
                        catch (IllegalArgumentException e)
                        {
                            throw new RuntimeException("Cannot format acquisition date", e);
                        }
                    }).orElse(DEFAULT_DATE);

            Element excitationElement = XMLUtil.getElement(imageElement, "MainExcitationWavelength");
            double imageExcitationWavelength = Double.parseDouble(excitationElement.getTextContent());

            Element emissionElement = XMLUtil.getElement(imageElement, "MainEmissionWavelength");
            double imageEmissionWavelength = Double.parseDouble(emissionElement.getTextContent());

            ColumbusImage image = new ColumbusImage();

            image.version = imageVersion;
            image.id = imageId;
            image.state = imageState;
            image.bufferNumber = imageBufferNumber;
            image.url = imageURL;
            image.row = imageRow;
            image.column = imageColumn;
            image.fieldId = imageFieldId;
            image.planeId = imagePlaneId;
            image.timepointId = imageTimepointId;
            image.channelId = imageChannelId;
            image.channelName = imageChannelName;
            image.channelColor = ColorUtil.getColorFromWavelength(imageEmissionWavelength);
            image.channelType = imageChannelType;
            image.acquisitionType = imageAcquisitionType;
            image.resolutionX = imageResolutionX;
            image.resolutionY = imageResolutionY;
            image.sizeX = imageSizeX;
            image.sizeY = imageSizeY;
            image.positionX = imagePositionX;
            image.positionY = -imagePositionY;
            image.positionZ = imagePositionZ;
            image.absPositionZ = imageAbsPositionZ;
            image.time = imageTime;
            image.date = imageDate;
            image.excitationWavelength = imageExcitationWavelength;
            image.emissionWavelength = imageEmissionWavelength;

            return image;
        }

    }

    private String version;
    private String id;
    private String state;
    private long bufferNumber;
    private String url;
    private long row;
    private long column;
    private long fieldId;
    private long planeId;
    private long timepointId;
    private long channelId;
    private String channelName;
    private Color channelColor;
    private String channelType;
    private String acquisitionType;
    private double resolutionX;
    private double resolutionY;
    private long sizeX;
    private long sizeY;
    private double positionX;
    private double positionY;
    private double absPositionZ;
    private double positionZ;
    private double time;
    private Date date;
    private double excitationWavelength;
    private double emissionWavelength;

    private ColumbusImage()
    {
    }

    public String getVersion()
    {
        return version;
    }

    public String getId()
    {
        return id;
    }

    public String getState()
    {
        return state;
    }

    public long getBufferNumber()
    {
        return bufferNumber;
    }

    public String getUrl()
    {
        return url;
    }

    public long getRow()
    {
        return row;
    }

    public long getColumn()
    {
        return column;
    }

    public long getFieldId()
    {
        return fieldId;
    }

    public long getPlaneId()
    {
        return planeId;
    }

    public long getTimepointId()
    {
        return timepointId;
    }

    public long getChannelId()
    {
        return channelId;
    }

    public String getChannelName()
    {
        return channelName;
    }

    public Color getChannelColor()
    {
        return channelColor;
    }

    public String getChannelType()
    {
        return channelType;
    }

    public String getAcquisitionType()
    {
        return acquisitionType;
    }

    public double getResolutionX()
    {
        return resolutionX;
    }

    public double getResolutionY()
    {
        return resolutionY;
    }

    public long getSizeX()
    {
        return sizeX;
    }

    public long getSizeY()
    {
        return sizeY;
    }

    public double getPositionX()
    {
        return positionX;
    }

    public double getPositionY()
    {
        return positionY;
    }

    public Rectangle2D getBounds()
    {
        return new Rectangle2D.Double(positionX, positionY, sizeX*resolutionX, sizeY*resolutionY);
    }

    public double getAbsPositionZ()
    {
        return absPositionZ;
    }

    public double getPositionZ()
    {
        return positionZ;
    }

    public double getTime()
    {
        return time;
    }

    public Date getDate()
    {
        return date;
    }

    public double getExcitationWavelength()
    {
        return excitationWavelength;
    }

    public double getEmissionWavelength()
    {
        return emissionWavelength;
    }

    @Override
    public String toString()
    {
        return "Image [url=" + url + ", bufferNumber=" + bufferNumber + ", row=" + row + ", column=" + column
                + ", resolutionX=" + resolutionX + ", resolutionY=" + resolutionY + ", sizeX=" + sizeX + ", sizeY="
                + sizeY + ", positionX=" + positionX + ", positionY=" + positionY + ", positionZ=" + positionZ + "]";
    }

}
