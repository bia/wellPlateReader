package danyfel80.wells.data;

import java.awt.Color;

public interface IChannel {
	long getId();

	String getName();

	Color getColor();

	double getExcitationWavelength();

	double getEmissionWavelength();

	IImage getImage();
}
