package danyfel80.wells.data.opera;

import org.w3c.dom.Element;

import icy.util.XMLUtil;

public class OperaLightSource {

	public static class Builder {
		public static OperaLightSource fromXML(Element lightSourceElement) {
			OperaLightSource lightSource = new OperaLightSource();

			lightSource.id = XMLUtil.getAttributeValue(lightSourceElement, "ID", null);
			lightSource.type = XMLUtil.getAttributeValue(lightSourceElement, "LightSourceType", null);
			lightSource.illumination = XMLUtil.getAttributeValue(lightSourceElement, "IlluminationType", null);
			lightSource.wavelength = XMLUtil.getElementDoubleValue(lightSourceElement, "Wavelength", 488);

			return lightSource;
		}
	}

	private String id;
	private String type;
	private String illumination;
	private double wavelength;

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public String getIllumination() {
		return illumination;
	}

	public double getWavelength() {
		return wavelength;
	}

}
