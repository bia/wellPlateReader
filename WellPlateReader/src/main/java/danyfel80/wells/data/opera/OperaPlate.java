package danyfel80.wells.data.opera;

import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.DoubleSupplier;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.xml.bind.DatatypeConverter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.drew.imaging.tiff.TiffMetadataReader;
import com.drew.imaging.tiff.TiffProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;

import danyfel80.wells.data.IPlate;
import danyfel80.wells.data.IWell;
import danyfel80.wells.util.MessageProgressListener;
import danyfel80.wells.util.stream.StreamUtils;
import icy.file.FileUtil;
import icy.util.XMLUtil;

public class OperaPlate implements IPlate
{

    public static class Builder
    {
        private String folder;
        private String[] flexFiles;
        private MessageProgressListener progressListener;

        public Builder(String folder, String[] flexFiles)
        {
            this.folder = folder;
            this.flexFiles = flexFiles;
            this.progressListener = null;
        }

        public Builder progressListener(MessageProgressListener progressListener)
        {
            this.progressListener = progressListener;
            return this;
        }

        private OperaPlate plate;

        public OperaPlate build() throws IOException
        {
            initializePlate();
            fillWellsData();
            return plate;
        }

        private void initializePlate() throws IOException
        {
            notifyProgress(() -> 0.01, () -> "Initializing plate");
            this.plate = new OperaPlate();
            Metadata flexMetadata;
            try
            {
                flexMetadata = TiffMetadataReader.readMetadata(new File(flexFiles[0]));
            }
            catch (TiffProcessingException e)
            {
                throw new IOException(e);
            }
            Directory flexDirectory = flexMetadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            // 0xfeb0 is the Tag containing the metadata xml.
            String flexDescriptionXml = flexDirectory.getDescription(0xfeb0);
            Document flexDocument;
            try
            {
                flexDocument = XMLUtil.createDocument(flexDescriptionXml);
            }
            catch (SAXException e)
            {
                throw new IOException("Could not read xml correctly", e);
            }

            Element rootElement = XMLUtil.getElement(flexDocument, "Root");
            Element flexElement = XMLUtil.getElement(rootElement, "FLEX");

            plate.folder = folder;
            plate.device = XMLUtil.getAttributeValue(flexElement, "OperaDevice", null);
            plate.version = XMLUtil.getAttributeValue(flexElement, "version", null);
            plate.lightSources = Optional.ofNullable(XMLUtil.getElement(flexElement, "LightSources"))
                    .map(sources -> XMLUtil.getElements(sources, "LightSource").stream()
                            .map(source -> OperaLightSource.Builder.fromXML(source))
                            .collect(Collectors.toMap(OperaLightSource::getId, Function.identity())))
                    .orElse(Collections.emptyMap());
            plate.filterSliders = Optional.ofNullable(XMLUtil.getElement(flexElement, "Filters"))
                    .map(filters -> XMLUtil.getElements(filters, "Slider").stream()
                            .map(sliderElement -> OperaFilterSlider.Builder.fromXML(sliderElement))
                            .collect(Collectors.toMap(OperaFilterSlider::getId, Function.identity())))
                    .orElse(Collections.emptyMap());
            plate.cameras = Optional.ofNullable(XMLUtil.getElement(flexElement, "Cameras"))
                    .map(cameras -> XMLUtil.getElements(cameras, "Camera").stream()
                            .map(cameraElement -> OperaCamera.Builder.fromXML(cameraElement))
                            .collect(Collectors.toMap(OperaCamera::getId, Function.identity())))
                    .orElse(Collections.emptyMap());

            plate.objectives = Optional.ofNullable(XMLUtil.getElement(flexElement, "Objectives"))
                    .map(objectives -> XMLUtil.getElements(objectives, "Objective").stream()
                            .map(objectiveElement -> OperaObjective.Buider.fromXML(objectiveElement))
                            .collect(Collectors.toMap(OperaObjective::getId, Function.identity())))
                    .orElse(Collections.emptyMap());
            plate.sublayouts = Optional.ofNullable(XMLUtil.getElement(flexElement, "Sublayouts"))
                    .map(sublayouts -> XMLUtil.getElements(sublayouts, "Sublayout").stream()
                            .map(sublayoutElement -> OperaSublayout.Builder.fromXML(sublayoutElement))
                            .collect(Collectors.toMap(OperaSublayout::getId, Function.identity())))
                    .orElse(Collections.emptyMap());

            plate.stacks = Optional.ofNullable(XMLUtil.getElement(flexElement, "Stacks"))
                    .map(stacks -> XMLUtil.getElements(stacks, "Stack").stream()
                            .map(stackElement -> OperaStack.Builder.fromXml(stackElement))
                            .collect(Collectors.toMap(OperaStack::getId, Function.identity())))
                    .orElse(Collections.emptyMap());
            plate.lightSourceCombinations = Optional
                    .ofNullable(XMLUtil.getElement(flexElement, "LightSourceCombinations"))
                    .map(combinations -> XMLUtil.getElements(combinations, "LightSourceCombination"))
                    .orElse(new ArrayList<>()).stream()
                    .collect(Collectors.toMap(combination -> XMLUtil.getAttributeValue(combination, "ID", null),
                            combination -> XMLUtil.getAttributeValue(XMLUtil.getElement(combination, "LightSourceRef"),
                                    "ID", null)));
            plate.filterCombinations = Optional.ofNullable(XMLUtil.getElement(flexElement, "FilterCombinations"))
                    .map(filterCombinations -> XMLUtil.getElements(filterCombinations, "FilterCombination"))
                    .orElse(new ArrayList<>()).stream()
                    .map(combinationElement -> OperaFilterCombination.Builder.fromXml(combinationElement))
                    .collect(Collectors.toMap(OperaFilterCombination::getId, Function.identity()));

            Optional<Element> plateElement = Optional.ofNullable(XMLUtil.getElement(flexElement, "Plate"));
            plate.name = plateElement.map(elem -> XMLUtil.getElementValue(elem, "PlateName", null)).orElse(null);
            plate.dimension = plateElement.map(elem -> new Dimension(XMLUtil.getElementIntValue(elem, "YSize", 0),
                    XMLUtil.getElementIntValue(elem, "XSize", 0))).orElse(new Dimension());
            plate.shape = plateElement
                    .map(elem -> OperaWellShape.Builder.fromXml(XMLUtil.getElement(elem, "WellShape")))
                    .orElse(OperaWellShape.defalutShape());
            plate.barcode = plateElement.map(elem -> XMLUtil.getElementValue(elem, "Barcode", null)).orElse(null);
            plate.startTime = plateElement.map(StreamUtils.wrapFunction(elem -> {
                String timeText = XMLUtil.getElementValue(elem, "StartTime", null);
                return timeText == null ? null : DatatypeConverter.parseDateTime(timeText).getTime();
            })).orElse(null);
            plate.operaMeasNumber = plateElement.map(elem -> XMLUtil.getElementIntValue(elem, "OperaMeasNo", 1))
                    .orElse(1);
        }

        private void fillWellsData()
        {
            notifyProgress(() -> 0.1, () -> "Reading flex files");
            final int flexFileCount = flexFiles.length;
            AtomicInteger processedFiles = new AtomicInteger();
            Map<Point, List<OperaWell>> wellsByCoordinate = Arrays.stream(flexFiles).parallel().map(e -> {  
                int fileNumber = processedFiles.incrementAndGet();
                notifyProgress(() -> 0.1 + 0.9 * fileNumber / flexFileCount,
                        () -> "Reading flex files (" + fileNumber + "/" + flexFileCount + ")");
                return e;
            }).map(StreamUtils.wrapFunction(file -> {
                return readWell(file);
            })).collect(Collectors.groupingBy(OperaWell::getPositionInPlate));
            plate.wells = wellsByCoordinate.entrySet().stream()
                    .map(coordinateWells -> OperaWell.joinWellsByField(coordinateWells.getValue(), plate.shape))
                    .collect(Collectors.toMap(OperaWell::getId, Function.identity(), (v1, v2) -> {
                        throw new RuntimeException(String.format("Duplicate key for values %s and %s", v1, v2));
                    }, TreeMap::new));
        }

        private OperaWell readWell(String file) throws IOException, TiffProcessingException
        {
            Metadata flexMetadata = TiffMetadataReader.readMetadata(new File(file));
            Directory flexDirectory = flexMetadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            // 0xfeb0 is the Tag containing the metadata xml.
            String flexDescriptionXml = flexDirectory.getDescription(0xfeb0);
            Document flexDocument;
            try
            {
                flexDocument = XMLUtil.createDocument(flexDescriptionXml);
            }
            catch (SAXException e)
            {
                throw new IOException("Could not read xml correctly", e);
            }

            Element rootElement = XMLUtil.getElement(flexDocument, "Root");
            Element flexElement = XMLUtil.getElement(rootElement, "FLEX");
            Element wellElement = XMLUtil.getElement(flexElement, "Well");
            return OperaWell.Builder.fromXml(wellElement, FileUtil.getFileName(file), plate.filterCombinations);
        }

        private void notifyProgress(DoubleSupplier progress, Supplier<String> message)
        {
            if (progressListener != null)
            {
                progressListener.notifyProgress(progress.getAsDouble(), message.get());
            }
        }

    }

    private String folder;
    private String device;
    private String version;

    private String name;
    private Dimension dimension;
    private OperaWellShape shape;
    private String barcode;
    private Date startTime;
    private int operaMeasNumber;

    private Map<String, OperaLightSource> lightSources;
    private Map<String, OperaFilterSlider> filterSliders;
    private Map<String, OperaCamera> cameras;
    private Map<String, OperaObjective> objectives;
    private Map<String, OperaSublayout> sublayouts;
    private Map<String, OperaStack> stacks;
    private Map<String, String> lightSourceCombinations; // Key=LightSourceCombiID, Value=LightSourceRefID
    private Map<String, OperaFilterCombination> filterCombinations;
    private Map<Long, OperaWell> wells;

    public String getFolder()
    {
        return folder;
    }

    @Override
    public String getId()
    {
        return barcode;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public String getType()
    {
        return name;
    }

    @Override
    public Dimension getDimension()
    {
        return dimension;
    }

    @Override
    public Map<Long, ? extends IWell> getWells()
    {
        return wells;
    }

    public String getDevice()
    {
        return device;
    }

    public String getVersion()
    {
        return version;
    }

    public OperaWellShape getShape()
    {
        return shape;
    }

    public String getBarcode()
    {
        return barcode;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public int getOperaMeasNumber()
    {
        return operaMeasNumber;
    }

    public Map<String, OperaLightSource> getLightSources()
    {
        return lightSources;
    }

    public OperaLightSource getLightSource(String id)
    {
        return lightSources.get(id);
    }

    public Map<String, OperaFilterSlider> getFilterSliders()
    {
        return filterSliders;
    }

    public OperaFilterSlider getFilterSlide(String id)
    {
        return filterSliders.get(id);
    }

    public Map<String, OperaCamera> getCameras()
    {
        return cameras;
    }

    public OperaCamera getCamera(String id)
    {
        return cameras.get(id);
    }

    public Map<String, OperaObjective> getObjectives()
    {
        return objectives;
    }

    public OperaObjective getObjective(String id)
    {
        return objectives.get(id);
    }

    public Map<String, OperaSublayout> getSublayouts()
    {
        return sublayouts;
    }

    public OperaSublayout getSublayout(String id)
    {
        return sublayouts.get(id);
    }

    public Map<String, OperaStack> getStacks()
    {
        return stacks;
    }

    public OperaStack getStack(String id)
    {
        return stacks.get(id);
    }

    public Map<String, String> getLightSourceCombinations()
    {
        return lightSourceCombinations;
    }

    public Map<String, OperaFilterCombination> getFilterCombinations()
    {
        return filterCombinations;
    }

    public OperaFilterCombination getFilterCombination(String id)
    {
        return filterCombinations.get(id);
    }

    @Override
    public String toString()
    {
        String plateID = getId();
        String plateType = getName();

        return plateID + " (" + plateType + ")";
    }
}
