package danyfel80.wells.data.opera;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.Date;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.w3c.dom.Element;

import danyfel80.wells.data.IImage;
import icy.type.dimension.Dimension2D;
import icy.util.ColorUtil;
import icy.util.XMLUtil;

public class OperaImage implements IImage
{

    public static class Builder
    {

        public static OperaImage fromXml(Element imageElement, String fileUrl,
                Map<String, OperaFilterCombination> filterCombinations)
        {
            OperaImage image = new OperaImage();
            image.buffer = XMLUtil.getAttributeIntValue(imageElement, "BufferNo", 0);
            image.fileName = fileUrl;
            image.objectiveId = XMLUtil.getElementValue(imageElement, "ObjectiveRef", null);
            image.filterCombinationId = XMLUtil.getElementValue(imageElement, "FilterCombinationRef", null);
            image.lightSourceCombinationId = XMLUtil.getElementValue(imageElement, "LightSourceCombinationRef", null);
            image.exposureId = XMLUtil.getElementIntValue(imageElement, "ExposureNo", 0);
            image.cameraExposureTime = XMLUtil.getElementIntValue(imageElement, "ExposureNo", 0);
            image.cameraBinning = new Dimension();
            image.cameraBinning.width = XMLUtil.getElementIntValue(imageElement, "CameraBinningX", 1);
            image.cameraBinning.height = XMLUtil.getElementIntValue(imageElement, "CameraBinningY", 1);
            image.plane = XMLUtil.getElementLongValue(imageElement, "Stack", 0);
            image.sublayoutId = XMLUtil.getElementLongValue(imageElement, "Sublayout", 0);
            image.focusHeight = XMLUtil.getElementDoubleValue(imageElement, "FocusHeight", 0d) * 1000000;
            image.absoluteFocusHeight = XMLUtil.getElementDoubleValue(imageElement, "AbsoluteFocusHeight", 0d)
                    * 1000000;
            image.absolutePiezoHeight = XMLUtil.getElementDoubleValue(imageElement, "AbsolutePiezoHeight", 0d)
                    * 1000000;
            image.relativeZ = XMLUtil.getElementDoubleValue(imageElement, "RelativeZ", 0d) * 1000000;
            image.positionXY = new Point2D.Double();
            image.positionXY.x = XMLUtil.getElementDoubleValue(imageElement, "PositionX", 0d) * 1000000;
            image.positionXY.y = XMLUtil.getElementDoubleValue(imageElement, "PositionY", 0d) * 1000000;
            image.positionZ = XMLUtil.getElementDoubleValue(imageElement, "PositionZ", 0d) * 1000000;
            image.measurementTimeOffset = XMLUtil.getElementDoubleValue(imageElement, "MeasurementTimeOffset", 0d);
            String timeText = XMLUtil.getElementValue(imageElement, "DateTime", null);
            try
            {
                image.time = timeText == null ? null : DatatypeConverter.parseDateTime(timeText).getTime();
            }
            catch (IllegalArgumentException e)
            {
                throw new RuntimeException(
                        "Cannot format start time " + timeText == null ? "" : timeText.replaceAll("Z$", "+0000"), e);
            }
            image.cameraReference = XMLUtil.getElementValue(imageElement, "CameraRef", null);
            image.type = XMLUtil.getElementValue(imageElement, "ImageType", null);
            image.channelName = XMLUtil.getElementValue(imageElement, "ChannelName", "");
            image.acquisitionType = XMLUtil.getElementValue(imageElement, "AcquisitionType", null);
            image.channelType = XMLUtil.getElementValue(imageElement, "ChannelType", "");
            image.illuminationType = XMLUtil.getElementValue(imageElement, "IlluminationType", "");
            image.imageFactor = XMLUtil.getElementDoubleValue(imageElement, "ImageFactor", 1d);
            image.bitsPerPixel = XMLUtil.getElementIntValue(imageElement, "ImageBitPerPixel", 16);
            image.size = new Dimension();
            image.size.width = XMLUtil.getElementIntValue(imageElement, "ImageWidth", 0);
            image.size.height = XMLUtil.getElementIntValue(imageElement, "ImageHeight", 0);
            image.compressionType = XMLUtil.getElementValue(imageElement, "CompressionType", "");
            image.compressionRate = XMLUtil.getElementDoubleValue(imageElement, "CompressionRate", 0d);
            image.resolution = new Dimension2D.Double();
            image.resolution.sizeX = XMLUtil.getElementDoubleValue(imageElement, "ImageResolutionX", 0d)*1000000;
            image.resolution.sizeY = XMLUtil.getElementDoubleValue(imageElement, "ImageResolutionY", 0d) * 1000000;

            OperaFilterCombination combi = filterCombinations.get(image.filterCombinationId);
            String cameraNumber = image.cameraReference.split("Cam")[1];
            String filter = combi.getSliderReferences().get("Camera" + cameraNumber);
            String[] filterValues = filter.split("/");
            image.excitation = Integer.parseInt(filterValues[0]) - Integer.parseInt(filterValues[1]);
            // Apply a very arbitrary Stoke shift
            image.emission = image.excitation + 40d;
            image.color = ColorUtil.getColorFromWavelength(image.emission);
            return image;
        }
    }

    private int buffer;
    private String fileName;
    private String objectiveId;
    private String filterCombinationId;
    private String lightSourceCombinationId;
    private int exposureId;
    private int cameraExposureTime;
    private Dimension cameraBinning;
    private long plane;
    private long sublayoutId;
    private double focusHeight;
    private double absoluteFocusHeight;
    private double absolutePiezoHeight;
    private double relativeZ;
    private Point2D.Double positionXY;
    private double positionZ;
    private double measurementTimeOffset;
    private Date time;
    private String cameraReference;
    private String type;
    private String channelName;
    private String acquisitionType;
    private String channelType;
    private String illuminationType;
    private double imageFactor;
    private int bitsPerPixel;
    private Dimension size;
    private String compressionType;
    private double compressionRate;
    private Dimension2D.Double resolution;
    private double excitation;
    private double emission;
    private Color color;

    private OperaImage()
    {
    }

    public String getObjectiveId()
    {
        return objectiveId;
    }

    public String getFilterCombinationId()
    {
        return filterCombinationId;
    }

    public String getLightSourceCombinationId()
    {
        return lightSourceCombinationId;
    }

    public int getExposureId()
    {
        return exposureId;
    }

    public int getCameraExposureTime()
    {
        return cameraExposureTime;
    }

    public Dimension getCameraBinning()
    {
        return cameraBinning;
    }

    public long getPlane()
    {
        return plane;
    }

    public long getSublayout()
    {
        return sublayoutId;
    }

    public double getFocusHeight()
    {
        return focusHeight;
    }

    public double getAbsoluteFocusHeight()
    {
        return absoluteFocusHeight;
    }

    public double getAbsolutePiezoHeight()
    {
        return absolutePiezoHeight;
    }

    public double getRelativeZ()
    {
        return relativeZ;
    }

    public Point2D.Double getPositionXY()
    {
        return positionXY;
    }

    public double getPositionZ()
    {
        return positionZ;
    }

    public double getMeasurementTimeOffset()
    {
        return measurementTimeOffset;
    }

    public Date getAcquisitionDate()
    {
        return time;
    }

    public String getCameraReference()
    {
        return cameraReference;
    }

    public String getType()
    {
        return type;
    }

    public String getChannelName()
    {
        return channelName;
    }

    public String getIlluminationType()
    {
        return illuminationType;
    }

    public double getImageFactor()
    {
        return imageFactor;
    }

    public int getBitsPerPixel()
    {
        return bitsPerPixel;
    }

    public Dimension getSize()
    {
        return size;
    }

    public String getCompressionType()
    {
        return compressionType;
    }

    public double getCompressionRate()
    {
        return compressionRate;
    }

    public Dimension2D.Double getResolution()
    {
        return resolution;
    }

    @Override
    public String getVersion()
    {
        return "1";
    }

    @Override
    public String getId()
    {
        return String.valueOf(buffer);
    }

    @Override
    public String getState()
    {
        return "Ok";
    }

    @Override
    public long getBufferNumber()
    {
        return buffer;
    }

    @Override
    public String getUrl()
    {
        return fileName;
    }

    @Override
    public long getRow()
    {
        // TODO retrieve from plate layout
        return -1;
    }

    @Override
    public long getColumn()
    {
        // TODO retrieve from plate layout
        return -1;
    }

    @Override
    public long getFieldId()
    {
        return sublayoutId;
    }

    @Override
    public long getPlaneId()
    {
        return plane;
    }

    @Override
    public long getTimepointId()
    {
        return 0;
    }

    @Override
    public long getChannelId()
    {
        return buffer;
    }

    @Override
    public Color getChannelColor()
    {
        return color;
    }

    @Override
    public String getChannelType()
    {
        return channelType;
    }

    @Override
    public String getAcquisitionType()
    {
        return acquisitionType;
    }

    @Override
    public double getResolutionX()
    {
        return resolution.sizeX;
    }

    @Override
    public double getResolutionY()
    {
        return resolution.sizeY;
    }

    @Override
    public long getSizeX()
    {
        return size.width;
    }

    @Override
    public long getSizeY()
    {
        return size.height;
    }

    @Override
    public double getPositionX()
    {
        return positionXY.x;
    }

    @Override
    public double getPositionY()
    {
        return positionXY.y;
    }

    @Override
    public double getAbsPositionZ()
    {
        return positionZ;
    }

    @Override
    public double getTime()
    {
        return measurementTimeOffset;
    }

    @Override
    public Date getDate()
    {
        return time;
    }

    @Override
    public double getExcitationWavelength()
    {
        return excitation;
    }

    @Override
    public double getEmissionWavelength()
    {
        return emission;
    }

}
