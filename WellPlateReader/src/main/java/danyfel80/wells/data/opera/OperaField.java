package danyfel80.wells.data.opera;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.w3c.dom.Element;

import danyfel80.wells.data.IField;
import danyfel80.wells.util.CollectionUtils;
import icy.util.XMLUtil;

public class OperaField implements IField
{

    public static class Builder
    {

        public static Map<Long, OperaField> fieldsFromXml(Element imagesElement, String fileUrl,
                Map<String, OperaFilterCombination> filterCombinations)
        {
            Map<Long, List<OperaImage>> imagesByField = XMLUtil.getElements(imagesElement, "Image").stream()
                    .map(imageElement -> OperaImage.Builder.fromXml(imageElement, fileUrl, filterCombinations))
                    .collect(Collectors.groupingBy(OperaImage::getSublayout));

            return imagesByField.entrySet().stream().map(
                    fieldEntry -> CollectionUtils.newEntry(fieldEntry.getKey(), createField(fieldEntry.getValue())))
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        }

        private static OperaField createField(List<OperaImage> fieldImages)
        {
            OperaField field = new OperaField();
            field.id = fieldImages.get(0).getSublayout();
            field.position = fieldImages.stream().findFirst().map(im -> im.getPositionXY())
                    .orElse(new Point2D.Double());
            field.planes = OperaPlane.Builder.fromImages(fieldImages);
            field.bounds = fieldImages.stream().findFirst()
                    .map(im -> new Rectangle2D.Double(im.getPositionX(), im.getPositionY(),
                            im.getSizeX() * im.getResolutionX(), im.getSizeY() * im.getResolutionY()))
                    .orElse(new Rectangle2D.Double());
            field.pixelBounds = fieldImages.stream().findFirst()
                    .map(im -> new Rectangle2D.Double(im.getPositionX() / im.getResolutionX(),
                            im.getPositionY() / im.getResolutionY(), im.getSizeX(), im.getSizeY()))
                    .orElse(new Rectangle2D.Double());
            return field;
        }

        /**
         * Integrates a given field to a given set of fields identified by their sublayoutId (their Id).
         * 
         * @param fieldToIntegrate
         *        Field to integrate.
         * @param integratedFields
         *        Integrated fields where the parameter field is integrated.
         */
        public static void integrateFieldToFields(OperaField fieldToIntegrate, Map<Long, OperaField> integratedFields)
        {
            OperaField integratedField = integratedFields.get(fieldToIntegrate.getId());
            if (integratedField == null)
            {
                integratedField = new OperaField();
                integratedField.id = fieldToIntegrate.id;
                integratedField.position = fieldToIntegrate.position;
                integratedField.planes = new HashMap<Long, OperaPlane>();
                integratedField.bounds = fieldToIntegrate.bounds;
                integratedFields.put(fieldToIntegrate.id, integratedField);
            }
            final Map<Long, OperaPlane> planes = integratedField.planes;
            fieldToIntegrate.planes.entrySet()
                    .forEach(planeEntry -> OperaPlane.Builder.integratePlaneToPlanes(planeEntry.getValue(), planes));
        }

    }

    private long id;
    private Point2D position;
    private Map<Long, OperaPlane> planes;
    private Rectangle2D bounds;
    private Rectangle2D pixelBounds;

    private OperaField()
    {
    }

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public Point2D getPosition()
    {
        return position;
    }

    @Override
    public Map<Long, OperaPlane> getPlanes()
    {
        return planes;
    }

    @Override
    public Rectangle2D getBounds()
    {
        return bounds;
    }

    @Override
    public Rectangle2D getPixelBounds()
    {
        return pixelBounds;
    }
}
