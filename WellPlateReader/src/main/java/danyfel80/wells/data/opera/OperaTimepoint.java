package danyfel80.wells.data.opera;

import java.util.HashMap;
import java.util.Map;

import danyfel80.wells.data.ITimepoint;

public class OperaTimepoint implements ITimepoint
{
    public static OperaTimepoint wrapChannels(Map<Long, OperaChannel> channels)
    {
        OperaTimepoint timepoint = new OperaTimepoint();
        timepoint.id = 0;
        timepoint.channels = channels;
        return timepoint;
    }

    public static void integrateTimepointToTimepoints(OperaTimepoint timepointToIntegrate,
            Map<Long, OperaTimepoint> integratedTimepoints)
    {
        OperaTimepoint integratedTimepoint = integratedTimepoints.get(timepointToIntegrate.getId());
        if (integratedTimepoint == null)
        {
            integratedTimepoint = new OperaTimepoint();
            integratedTimepoint.id = timepointToIntegrate.id;
            integratedTimepoint.channels = new HashMap<>();
            integratedTimepoints.put(timepointToIntegrate.id, integratedTimepoint);
        }
        final Map<Long, OperaChannel> integratedChannels = integratedTimepoint.channels;
        timepointToIntegrate.channels.entrySet().forEach(channelEntry -> OperaChannel.Builder
                .integrateChannelToChannels(channelEntry.getValue(), integratedChannels));
    }

    private long id;
    private Map<Long, OperaChannel> channels;

    private OperaTimepoint()
    {
    }

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public Map<Long, OperaChannel> getChannels()
    {
        return channels;
    }

}
