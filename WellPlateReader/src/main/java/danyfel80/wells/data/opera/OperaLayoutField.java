package danyfel80.wells.data.opera;

import java.awt.geom.Point2D;

import org.w3c.dom.Element;

import icy.util.XMLUtil;

public class OperaLayoutField
{
    public static class Builder
    {

        public static OperaLayoutField fromXML(Element fieldElement)
        {
            OperaLayoutField field = new OperaLayoutField();
            field.id = XMLUtil.getAttributeIntValue(fieldElement, "No", 0);
            field.offset = new Point2D.Double();
            Element offsetXElement = XMLUtil.getElement(fieldElement, "OffsetX");
            field.offset.x = Double.parseDouble(offsetXElement.getTextContent());
            if (XMLUtil.getAttributeValue(offsetXElement, "Unit", "mm").equals("m"))
            {
                field.offset.x *= 1000000;
            }
            Element offsetYElement = XMLUtil.getElement(fieldElement, "OffsetY");
            field.offset.y = Double.parseDouble(offsetYElement.getTextContent());
            if (XMLUtil.getAttributeValue(offsetYElement, "Unit", "m").equals("m"))
            {
                field.offset.y *= 1000000;
            }
            return field;
        }

    }

    private int id;
    private Point2D.Double offset;

    private OperaLayoutField()
    {
    }

    public int getId()
    {
        return id;
    }

    public Point2D.Double getOffset()
    {
        return offset;
    }

}
