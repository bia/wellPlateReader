package danyfel80.wells.data.opera;

import org.w3c.dom.Element;

import icy.type.dimension.Dimension2D;
import icy.util.XMLUtil;

public class OperaWellShape
{

    public static class Builder
    {

        public static OperaWellShape fromXml(Element shapeElement)
        {
            OperaWellShape shape = new OperaWellShape();
            shape.shape = XMLUtil.getElementValue(shapeElement, "Shape", "Circle");
            Element xSizeElem = XMLUtil.getElement(shapeElement, "XSize");
            Element ySizeElem = XMLUtil.getElement(shapeElement, "YSize");
            shape.size = new Dimension2D.Double(XMLUtil.getDoubleValue(xSizeElem, 0d),
                    XMLUtil.getDoubleValue(ySizeElem, 0d));
            if (XMLUtil.getAttributeValue(xSizeElem, "Unit", "m").equals("m"))
            {
                shape.size.sizeX *= 1000000;
            }
            if (XMLUtil.getAttributeValue(ySizeElem, "Unit", "m").equals("m"))
            {
                shape.size.sizeY *= 1000000;
            }
            return shape;
        }

    }

    public static OperaWellShape defalutShape()
    {
        OperaWellShape shape = new OperaWellShape();
        shape.shape = "Circle";
        shape.size = new Dimension2D.Double();
        return shape;
    }

    private String shape;
    private Dimension2D.Double size;

    public String getShape()
    {
        return shape;
    }

    public Dimension2D.Double getSize()
    {
        return size;
    }

}
