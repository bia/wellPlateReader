package danyfel80.wells.data.opera;

import org.w3c.dom.Element;

import danyfel80.wells.util.MeasureUtil;
import icy.type.dimension.Dimension2D;
import icy.util.XMLUtil;

public class OperaCamera
{

    public static class Builder
    {

        public static OperaCamera fromXML(Element cameraElement)
        {
            OperaCamera camera = new OperaCamera();
            camera.type = XMLUtil.getAttributeValue(cameraElement, "CameraType", null);
            camera.id = XMLUtil.getAttributeValue(cameraElement, "ID", null);
            camera.pixelSize = new Dimension2D.Double();
            Element pixelSizeXElement = XMLUtil.getElement(cameraElement, "PixelSizeX");
            camera.pixelSize.sizeX = MeasureUtil.getLengthInMicrons(XMLUtil.getDoubleValue(pixelSizeXElement, 1d),
                    XMLUtil.getAttributeValue(pixelSizeXElement, "Unit", "�m"));
            Element pixelSizeYElement = XMLUtil.getElement(cameraElement, "PixelSizeY");
            camera.pixelSize.sizeY = MeasureUtil.getLengthInMicrons(XMLUtil.getDoubleValue(pixelSizeYElement, 1d),
                    XMLUtil.getAttributeValue(pixelSizeYElement, "Unit", "�m"));

            return camera;
        }

    }

    private String id;
    private String type;
    private Dimension2D.Double pixelSize;

    public String getId()
    {
        return id;
    }

    public String getType()
    {
        return type;
    }

    public Dimension2D.Double getPixelSize()
    {
        return pixelSize;
    }

}
