package danyfel80.wells.data.opera;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import org.w3c.dom.Element;

import icy.util.XMLUtil;

public class OperaStack
{

    public static class Builder
    {

        public static OperaStack fromXml(Element stackElement)
        {
            OperaStack stack = new OperaStack();
            stack.id = XMLUtil.getAttributeValue(stackElement, "ID", null);
            stack.planes = Collections.unmodifiableMap(XMLUtil.getElements(stackElement, "Plane").stream().collect(
                    Collectors.toMap(planeElem -> XMLUtil.getAttributeIntValue(planeElem, "No", 0), planeElem -> {
                        double zOffset = XMLUtil.getElementDoubleValue(planeElem, "OffsetZ", 0d);
                        if (XMLUtil.getAttributeValue(XMLUtil.getElement(planeElem, "OffsetZ"), "Unit", "m")
                                .equals("m"))
                        {
                            zOffset *= 1000000;
                        }
                        return zOffset;
                    })));
            return stack;
        }

    }

    private String id;
    private Map<Integer, Double> planes;

    public String getId()
    {
        return id;
    }

    public Map<Integer, Double> getPlanes()
    {
        return planes;
    }

}
