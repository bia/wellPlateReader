package danyfel80.wells.data.opera;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import danyfel80.wells.data.IPlane;
import danyfel80.wells.util.CollectionUtils;

public class OperaPlane implements IPlane
{

    public static class Builder
    {

        // public static Map<Long, OperaPlane> planesFromXml(Element imagesElement)
        // {
        // Map<Long, List<OperaImage>> imagesByPlane = XMLUtil.getElements(imagesElement, "Image").stream()
        // .map(OperaImage.Builder::fromXml).collect(Collectors.groupingBy(OperaImage::getPlane));
        // Map<Long, OperaPlane> planes = imagesByPlane.entrySet().stream()
        // .map(plEntry -> CollectionUtils.newEntry(plEntry.getKey(), planeFromImages(plEntry.getValue())))
        // .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        // return planes;
        // }

        private static OperaPlane planeFromImages(List<OperaImage> planeImages)
        {
            OperaPlane plane = new OperaPlane();
            plane.id = planeImages.get(0).getPlane();
            plane.positionZ = planeImages.get(0).getRelativeZ();
            Map<Long, OperaChannel> channels = OperaChannel.Builder.channelsFromImages(planeImages);
            plane.timepoints = new HashMap<Long, OperaTimepoint>();
            plane.timepoints.put(0L, OperaTimepoint.wrapChannels(channels));
            return plane;
        }

        public static Map<Long, OperaPlane> fromImages(List<OperaImage> fieldImages)
        {
            Map<Long, List<OperaImage>> imagesByPlane = fieldImages.stream()
                    .collect(Collectors.groupingBy(OperaImage::getPlane));
            Map<Long, OperaPlane> planes = imagesByPlane.entrySet().stream()
                    .map(plEntry -> CollectionUtils.newEntry(plEntry.getKey(), planeFromImages(plEntry.getValue())))
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
            return planes;
        }

        public static void integratePlaneToPlanes(OperaPlane planeToIntegrate, Map<Long, OperaPlane> integratedPlanes)
        {
            OperaPlane integratedPlane = integratedPlanes.get(planeToIntegrate.getId());
            if (integratedPlane == null)
            {
                integratedPlane = new OperaPlane();
                integratedPlane.id = planeToIntegrate.id;
                integratedPlane.positionZ = planeToIntegrate.positionZ;
                integratedPlane.timepoints = new HashMap<>();
                integratedPlanes.put(planeToIntegrate.id, integratedPlane);
            }
            final Map<Long, OperaTimepoint> integratedTimepoints = integratedPlane.timepoints;
            planeToIntegrate.timepoints.entrySet().forEach(timeEntry -> OperaTimepoint
                    .integrateTimepointToTimepoints(timeEntry.getValue(), integratedTimepoints));
        }

    }

    private long id;
    private double positionZ;
    private Map<Long, OperaTimepoint> timepoints;

    private OperaPlane()
    {
    }

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public double getPositionZ()
    {
        return positionZ;
    }

    @Override
    public Map<Long, OperaTimepoint> getTimepoints()
    {
        return timepoints;
    }

}
