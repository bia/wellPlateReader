package danyfel80.wells.data.opera;

import java.util.Map;
import java.util.stream.Collectors;

import org.w3c.dom.Element;

import icy.util.XMLUtil;

public class OperaFilterCombination
{

    public static class Builder
    {

        public static OperaFilterCombination fromXml(Element filterCombinationElement)
        {
            OperaFilterCombination combination = new OperaFilterCombination();
            combination.id = XMLUtil.getAttributeValue(filterCombinationElement, "ID", null);
            combination.sliderReferences = XMLUtil.getElements(filterCombinationElement, "SliderRef").stream()
                    .collect(Collectors.toMap(
                            sliderRefElement -> XMLUtil.getAttributeValue(sliderRefElement, "ID", null),
                            sliderRefElement -> XMLUtil.getAttributeValue(sliderRefElement, "Filter", null)));
            return combination;
        }

    }

    private String id;
    private Map<String, String> sliderReferences; // key=SliderID, value=FilterID

    private OperaFilterCombination()
    {
    }

    public String getId()
    {
        return id;
    }

    public Map<String, String> getSliderReferences()
    {
        return sliderReferences;
    }

}
