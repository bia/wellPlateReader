package danyfel80.wells.data.opera;

import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

import danyfel80.wells.data.IField;
import danyfel80.wells.data.IWell;
import icy.type.dimension.Dimension2D;
import icy.util.XMLUtil;

public class OperaWell implements IWell
{
    public static class Builder
    {

        public static OperaWell fromXml(Element wellElement, String fileUrl,
                Map<String, OperaFilterCombination> filterCombinations)
        {
            OperaWell well = new OperaWell();
            well.name = XMLUtil.getElementValue(wellElement, "AreaName", "Unknown");
            well.coordinate = new Point();
            Element coordinateElement = XMLUtil.getElement(wellElement, "WellCoordinate");
            well.coordinate.x = XMLUtil.getAttributeIntValue(coordinateElement, "Col", 0);
            well.coordinate.y = XMLUtil.getAttributeIntValue(coordinateElement, "Row", 0);
            well.sublayoutId = XMLUtil.getElementValue(wellElement, "SublayoutRef", null);
            well.stackId = XMLUtil.getElementValue(wellElement, "StackRef", null);

            well.fields = OperaField.Builder.fieldsFromXml(XMLUtil.getElement(wellElement, "Images"), fileUrl,
                    filterCombinations);
            well.wellId = well.coordinate.y*1000 + well.coordinate.x;
            return well;
        }
    }

    /**
     * @param wellsToIntegrate
     *        Set of wells to join into a single integrated well.
     * @param plateWellShape
     * @return Integrated well joining all fields on input wells.
     */
    public static OperaWell joinWellsByField(List<OperaWell> wellsToIntegrate, OperaWellShape plateWellShape)
    {
        OperaWell integratedWell = new OperaWell();
        { // Take info from first well
            OperaWell firstWell = wellsToIntegrate.get(0);
            integratedWell.name = firstWell.name;
            integratedWell.coordinate = firstWell.coordinate;
            integratedWell.sublayoutId = firstWell.sublayoutId;
            integratedWell.stackId = firstWell.stackId;
            integratedWell.fields = new HashMap<>();
            integratedWell.wellId = firstWell.wellId;
        }

        wellsToIntegrate.stream()
                .forEach(wellToIntegrate -> integrateWellFields(wellToIntegrate.fields, integratedWell.fields));

        Rectangle2D wellRectangle = integratedWell.fields.values().stream().map(f -> f.getBounds())
                .reduce(new Rectangle2D.Double(), (acc, elem) -> {
                    if (acc.isEmpty())
                        acc.setRect(elem);
                    else if (!elem.isEmpty())
                        acc.add(elem);
                    return acc;
                });

        if (plateWellShape.getSize().getWidth() > 0 && plateWellShape.getSize().getHeight() > 0)
        {
            Dimension2D.Double plateWellSize = plateWellShape.getSize();
            wellRectangle.setRect(-plateWellSize.sizeX / 2d, -plateWellSize.sizeY / 2d, plateWellSize.sizeX,
                    plateWellSize.sizeY);
        }

        if (plateWellShape.getShape().equals("Circle"))
        {
            integratedWell.shape = new OperaSingleWellShape(new Ellipse2D.Double(wellRectangle.getX(),
                    wellRectangle.getY(), wellRectangle.getWidth(), wellRectangle.getHeight()));
        }
        else
        {
            integratedWell.shape = new OperaSingleWellShape(wellRectangle);
        }

        return integratedWell;
    }

    private static void integrateWellFields(Map<Long, OperaField> wellFields, Map<Long, OperaField> integratedFields)
    {
        wellFields.entrySet().stream().forEach(
                fieldEntry -> OperaField.Builder.integrateFieldToFields(fieldEntry.getValue(), integratedFields));
    }

    private long wellId;
    private String name;
    private Point coordinate;
    private String sublayoutId;
    private String stackId;
    private Map<Long, OperaField> fields;
    private OperaSingleWellShape shape;

    private OperaWell()
    {
    }

    public String getName()
    {
        return name;
    }

    @Override
    public long getId()
    {
        return wellId;
    }

    @Override
    public Point getPositionInPlate()
    {
        return coordinate;
    }

    public String getSublayoutId()
    {
        return sublayoutId;
    }

    public String getStackId()
    {
        return stackId;
    }

    @Override
    public Map<Long, OperaField> getFields()
    {
        return fields;
    }

    @Override
    public OperaSingleWellShape getShape()
    {
        return shape;
    }

    @Override
    public Rectangle2D getFieldBoundsOnWell(IField field)
    {
        Rectangle2D fieldBounds = field.getBounds();
        Rectangle2D wellBounds = getShape().getShape().getBounds2D();
        // reusing created rectangle to store field adjusted bounds.
        wellBounds.setRect(wellBounds.getCenterX() + (fieldBounds.getX() - (0.5 * fieldBounds.getWidth())),
                wellBounds.getCenterY() + fieldBounds.getY() - (0.5 * fieldBounds.getHeight()), fieldBounds.getWidth(),
                fieldBounds.getHeight());
        return wellBounds;
    }
}
