package danyfel80.wells.data.opera;

import org.w3c.dom.Element;

import icy.util.XMLUtil;

public class OperaObjective
{

    public static class Buider
    {

        public static OperaObjective fromXML(Element objectiveElement)
        {
            OperaObjective objective = new OperaObjective();
            objective.id = XMLUtil.getAttributeValue(objectiveElement, "ID", null);
            objective.magnification = XMLUtil.getElementDoubleValue(objectiveElement, "Magnification", 1d);
            objective.aperture = XMLUtil.getElementDoubleValue(objectiveElement, "NumAperture", 1d);
            objective.immersion = XMLUtil.getElementDoubleValue(objectiveElement, "Immersion", 1d);
            return objective;
        }

    }

    private String id;
    private double magnification;
    private double aperture;
    private double immersion;

    private OperaObjective()
    {
    }

    public String getId()
    {
        return id;
    }

    public double getMagnification()
    {
        return magnification;
    }

    public double getAperture()
    {
        return aperture;
    }

    public double getImmersion()
    {
        return immersion;
    }

}
