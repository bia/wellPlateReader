/**
 * 
 */
package danyfel80.wells.data.opera;

import java.awt.Shape;

import danyfel80.wells.data.IWellShape;

/**
 * Single well shape for opera flex microscopes.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class OperaSingleWellShape implements IWellShape
{
    private Shape shape;

    public OperaSingleWellShape(Shape shape)
    {
        this.shape = shape;
    }

    @Override
    public Shape getShape()
    {
        return shape;
    }

}
