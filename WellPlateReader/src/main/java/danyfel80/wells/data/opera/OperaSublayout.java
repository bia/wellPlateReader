package danyfel80.wells.data.opera;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.w3c.dom.Element;

import icy.util.XMLUtil;

public class OperaSublayout
{

    public static class Builder
    {

        public static OperaSublayout fromXML(Element sublayoutElement)
        {
            OperaSublayout sublayout = new OperaSublayout();
            sublayout.id = XMLUtil.getAttributeValue(sublayoutElement, "ID", null);
            sublayout.fields = Collections.unmodifiableMap(XMLUtil.getElements(sublayoutElement, "Field").stream()
                    .map(fieldElement -> OperaLayoutField.Builder.fromXML(fieldElement))
                    .collect(Collectors.toMap(OperaLayoutField::getId, Function.identity())));
            return sublayout;
        }

    }

    private String id;
    private Map<Integer, OperaLayoutField> fields;

    private OperaSublayout()
    {
    }

    public String getId()
    {
        return id;
    }

    public Map<Integer, OperaLayoutField> getFields()
    {
        return fields;
    }

}
