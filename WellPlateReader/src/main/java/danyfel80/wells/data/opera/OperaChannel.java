package danyfel80.wells.data.opera;

import java.awt.Color;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import danyfel80.wells.data.IChannel;

public class OperaChannel implements IChannel
{

    public static class Builder
    {
        public static Map<Long, OperaChannel> channelsFromImages(List<OperaImage> planeImages)
        {
            return planeImages.stream().collect(Collectors.toMap(OperaImage::getBufferNumber, im -> fromImage(im)));
        }

        public static OperaChannel fromImage(OperaImage channelImage)
        {
            OperaChannel channel = new OperaChannel();
            channel.id = channelImage.getBufferNumber();
            channel.name = channelImage.getChannelName();
            channel.type = channelImage.getChannelType();
            channel.image = channelImage;
            return channel;
        }

        public static void integrateChannelToChannels(OperaChannel channelToIntegrate,
                Map<Long, OperaChannel> integratedChannels)
        {
            OperaChannel integratedChannel = integratedChannels.get(channelToIntegrate.getId());
            if (integratedChannel == null)
            {
                integratedChannel = new OperaChannel();
                integratedChannel.id = channelToIntegrate.id;
                integratedChannel.name = channelToIntegrate.name;
                integratedChannel.type = channelToIntegrate.type;
                integratedChannel.image = channelToIntegrate.image;
                integratedChannels.put(channelToIntegrate.id, integratedChannel);
            }
        }
    }

    private long id;
    private String name;
    private String type;
    private OperaImage image;

    private OperaChannel()
    {
    }

    @Override
    public long getId()
    {
        return id;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public String getType()
    {
        return type;
    }

    @Override
    public Color getColor()
    {
        return image.getChannelColor();
    }

    @Override
    public double getExcitationWavelength()
    {
        return image.getExcitationWavelength();
    }

    @Override
    public double getEmissionWavelength()
    {
        return image.getEmissionWavelength();
    }

    @Override
    public OperaImage getImage()
    {
        return image;
    }

}
