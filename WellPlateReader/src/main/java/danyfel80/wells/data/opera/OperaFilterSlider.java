package danyfel80.wells.data.opera;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import org.w3c.dom.Element;

import icy.util.XMLUtil;

public class OperaFilterSlider
{

    public static class Builder
    {

        public static OperaFilterSlider fromXML(Element sliderElement)
        {
            OperaFilterSlider filterSlider = new OperaFilterSlider();
            filterSlider.id = XMLUtil.getAttributeValue(sliderElement, "Name", null);
            filterSlider.filters = Collections.unmodifiableSet(XMLUtil.getElements(sliderElement, "Filter").stream()
                    .map(filterElement -> XMLUtil.getAttributeValue(filterElement, "ID", "540/75"))
                    .collect(Collectors.toSet()));

            return filterSlider;
        }

    }

    private String id;
    private Set<String> filters;

    private OperaFilterSlider()
    {
    }

    public String getId()
    {
        return id;
    }

    public Set<String> getFilters()
    {
        return filters;
    }

}
