package danyfel80.wells.data;

import java.awt.Color;
import java.util.Date;

public interface IImage {
	public String getVersion();

	public String getId();

	public String getState();

	public long getBufferNumber();

	public String getUrl();

	public long getRow();

	public long getColumn();

	public long getFieldId();

	public long getPlaneId();

	public long getTimepointId();

	public long getChannelId();

	public Color getChannelColor();

	public String getChannelType();

	public String getAcquisitionType();

	public double getResolutionX();

	public double getResolutionY();

	public long getSizeX();

	public long getSizeY();

	public double getPositionX();

	public double getPositionY();

	public double getAbsPositionZ();

	public double getPositionZ();

	public double getTime();

	public Date getDate();

	public double getExcitationWavelength();

	public double getEmissionWavelength();
}
